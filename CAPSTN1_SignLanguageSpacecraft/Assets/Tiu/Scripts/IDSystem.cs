using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDSystem : MonoBehaviour
{
    public char currentID;
    public int Points;

    // Start is called before the first frame update
    void Start()
    {
        GenerateID();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateID()
    {
        // Assigns a random letter
        currentID = SymbolManager.instance.GetRandomLetter().ID();
        
        // Testing purpose
        //currentID = 'a';
        Debug.Log("ID: " + currentID);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Collectible>().symbol.CompareID(currentID))
        {
            Destroy(col.gameObject);
            Points++;

            if (Points > 1)
            {
                Win();
            }
        }
        else
        {
            GetComponent<PlayerController>().TakeDamage(1);
        }
    }

    void Win()
    {
        Debug.Log("You are winner");
    }
}
