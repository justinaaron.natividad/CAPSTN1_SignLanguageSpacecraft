using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public DialogueManager manager;
    public Message[] messages;
    public Speaker[] speakers;

    public void StartDisplay()
    {
        manager.StartDialogue(messages, speakers);
    }
}

[System.Serializable]

public class Speaker
{
    public Sprite SpeakerImage;
    public string SpeakerName;
}

[System.Serializable]
public class Message
{
    public int SpeakerID;
    
    [TextArea(3, 10)]
    public string message;
}