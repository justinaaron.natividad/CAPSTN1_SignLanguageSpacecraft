using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    #region Public Variables
    public static DialogueManager instance;
    public GameObject DialoguePanel;
    //public GameObject StartButton;

    [Header("Speaker Data")] 
    public Image SpeakerImage;
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI MessageText;
    #endregion

    #region Private Variables
    [Header("Current Message Data")]
    [SerializeField] private Message[] CurrentMessages;
    private Speaker[] CurrentSpeakers;
    private int ActiveMessageID;
    #endregion

    #region Delegate
    public delegate void OnDialogueFinished();
    public static OnDialogueFinished DialogueFinished;
    #endregion

    void Awake()
    {
        // Singleton
        instance = this;
    }

    private void OnEnable()
    {
        DialogueFinished += EndDialogue;
    }

    private void OnDisable()
    {
        DialogueFinished -= EndDialogue;
    }

    public void StartDialogue(Message[] messages, Speaker[] speakers)
    {
        // Assigning values
        CurrentMessages = messages;
        CurrentSpeakers = speakers;
        ActiveMessageID = 0;
        
        // Bring up the Dialogue UI
        if (!DialoguePanel.activeSelf) DialoguePanel.SetActive(true);

        DisplayMessage();
    }

    public void DisplayMessage()
    {
        if (CurrentMessages.Length > 0)
        {
            // Display the current message from CurrentMessage at index ActiveMessageID
            Message MessageToDisplay = CurrentMessages[ActiveMessageID];
            MessageText.text = MessageToDisplay.message;

            // Displays who's saying the message
            Speaker SpeakerToDisplay = CurrentSpeakers[MessageToDisplay.SpeakerID];
            NameText.text = SpeakerToDisplay.SpeakerName;
            SpeakerImage.sprite = SpeakerToDisplay.SpeakerImage;
        }
    }

    // Plays the next Message in the Queue
    public void NextMessage()
    {
        // Check if there are messages
        if (CurrentMessages.Length > 0)
        {
            ActiveMessageID++;
            if (ActiveMessageID < CurrentMessages.Length)
            {
                DisplayMessage();
            }
            else
            {
                DialogueFinished?.Invoke();
            }
        }
    }
    
    public void SkipDialogue()
    {
        // Sets the current ID to the last one and calls the NextMessage function to disable the dialogue box
        ActiveMessageID = CurrentMessages.Length;
        NextMessage();
    }

    void EndDialogue()
    {
        Debug.Log("No more messages");
        DialoguePanel.SetActive(false);
        //StartButton.SetActive(false);
    }
}
