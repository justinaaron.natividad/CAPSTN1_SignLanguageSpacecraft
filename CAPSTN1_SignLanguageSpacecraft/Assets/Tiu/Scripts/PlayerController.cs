using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool CanMove;
    
    private Rigidbody2D rib2d;
    [SerializeField] private Joystick joystick;

    [Header("Player Stats")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotationLerpValue;

    [Header("Player Health")] 
    [SerializeField] private int MaxHP;
    public int CurrentHP;
    
    // Start is called before the first frame update
    void Start()
    {
        rib2d = GetComponent<Rigidbody2D>();
        CurrentHP = MaxHP;
        CanMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (joystick.GetMovementValue() != Vector2.zero && CanMove) // for floaty movement
        {
            rib2d.velocity = joystick.GetMovementValue() * moveSpeed;
            transform.up = Vector2.Lerp(transform.up, joystick.GetMovementValue(), rotationLerpValue);
        }
    }

    public void TakeDamage(int damage)
    {
        CurrentHP -= damage;

        if (CurrentHP <= 0)
        {
            CanMove = false;
            Debug.Log("Game Over");
        }
    }
}
