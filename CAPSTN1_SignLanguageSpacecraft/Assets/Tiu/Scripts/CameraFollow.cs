using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private GameObject target;

    private float timeOffset = 3f;
    [SerializeField] private Vector2 posOffset = new Vector2(1f, 0.5f);
    [SerializeField] private Vector2 maxClamp = new Vector2(21f, 22f);

    private Vector3 startPos, endPos;

    private void FixedUpdate()
    {
        startPos = transform.position;
        endPos = target.transform.position;

        endPos.x += posOffset.x;
        endPos.y += posOffset.y;
        endPos.z = -10;

        // Follow player
        transform.position = Vector3.Lerp(startPos, endPos, timeOffset * Time.deltaTime);

        transform.position = new Vector3
        (
            Mathf.Clamp(transform.position.x, -maxClamp.x, maxClamp.x),
            Mathf.Clamp(transform.position.y, -maxClamp.y, maxClamp.y),
            transform.position.z
        );
    }
    
    // Visualize Boundary
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        // Top, Right, Bottom, Left Sides
        Gizmos.DrawLine(new Vector2(-maxClamp.x, maxClamp.y), new Vector2(maxClamp.x, maxClamp.y));
        Gizmos.DrawLine(new Vector2(maxClamp.x, maxClamp.y), new Vector2(maxClamp.x, -maxClamp.y));
        Gizmos.DrawLine(new Vector2(-maxClamp.x, -maxClamp.y), new Vector2(maxClamp.x, -maxClamp.y));
        Gizmos.DrawLine(new Vector2(-maxClamp.x, maxClamp.y), new Vector2(-maxClamp.x, -maxClamp.y));
    }
}
