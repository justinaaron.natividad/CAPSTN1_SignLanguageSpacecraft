using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private Vector2 spawnPos;

    [SerializeField] private float offsetRadius;

    [SerializeField] private int numberToSpawn;
    [SerializeField] private int borderX = 25;
    [SerializeField] private int borderY = 25;

    [SerializeField] private GameObject ObjectToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawn()
    {
        for (int i = 0; i < numberToSpawn; i++)
        {
            // Make sure the spawned objects are not too near to another object
            do
            {
                spawnPos.x = Random.Range(-borderX, borderX);
                spawnPos.y = Random.Range(-borderY, borderY);
            } while (Physics2D.OverlapCircle(spawnPos, offsetRadius));
            
            GameObject NewSymbol = Instantiate(ObjectToSpawn, spawnPos, Quaternion.identity, transform);
        } 
    }

    public void Rescan()
    {
        int i = 0;
        
        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);

            Debug.Log("Destroyed: " + (i + 1) + " objects");
            i++;
        }
        
        Spawn();
    }
}
