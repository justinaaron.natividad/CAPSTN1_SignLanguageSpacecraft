using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSymbol : MonoBehaviour
{
    public GameObject symbolToSpawn;
    
    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Test function to see if spawning symbols work
    void Spawn()
    {
        GameObject NewObject = Instantiate(symbolToSpawn, transform.position, Quaternion.identity);
        
        // Check if the symbol was spawned
        if (NewObject)
        {
            Debug.Log("Success");
        }
        else Debug.Log("Error");
    }
}
