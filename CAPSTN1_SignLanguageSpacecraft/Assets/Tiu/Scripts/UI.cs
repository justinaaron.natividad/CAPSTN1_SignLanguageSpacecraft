using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI : MonoBehaviour
{
    [SerializeField] private GameObject player;
    public TextMeshProUGUI letterText;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI pointsText;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        letterText.text = "Current Letter: " + player.GetComponent<IDSystem>().currentID;
        healthText.text = "Health: " + player.GetComponent<PlayerController>().CurrentHP;
        pointsText.text = "Collected: " + player.GetComponent<IDSystem>().Points;
    }
}
