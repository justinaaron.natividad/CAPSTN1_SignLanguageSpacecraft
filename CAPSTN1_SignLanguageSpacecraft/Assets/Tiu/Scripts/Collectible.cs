using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public Symbol symbol;

    private void Awake()
    {
        symbol = SymbolManager.instance.GetRandomLetter();
        
        UpdateData();
    }

    public void UpdateData()
    {
        GetComponent<SpriteRenderer>().sprite = symbol.Image();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
