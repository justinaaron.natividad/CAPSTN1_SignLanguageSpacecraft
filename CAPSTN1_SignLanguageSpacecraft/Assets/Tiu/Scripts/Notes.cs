// Notes on the current scripts.

// CameraFollow.cs
// Attached to the camera.
// This script is mainly for the camera to follow the player as they move through the area.

// Collectible.cs
// Attached to the objects that the player has to collect.
// As of now, this script assigns a random symbol to the object.

// IDSystem.cs
// Attached to the player.
// Currently, this script generates a random letter on the player and compares letters when the player collides with a 
// collectible. If it matches, the collectible is destroyed and the player earns a point. Collect enough points to win.
// Alternatively, if the player id and the object id does not match, the player loses 1 HP. Losing all HP ends the game.

// PlayerController.cs
// Attached to the player.
// Responsible for making the player move. Also currently responsible for managing player HP.

// Spawner.cs
// Attached to the spawner object.
// Spawns a random symbol in a random part of the map, making sure that there is enough space between symbols.
// Contains a Rescan function that destroys all current symbols then generates a new set.

// UI.cs
// Attached to the UI Manager.
// Handles UI.