using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SymbolScript : MonoBehaviour
{
    public Symbol symbol;
    
    // Start is called before the first frame update
    void Start()
    {
        // If successfully spawned, change the sprite to the one that's assigned to the symbol
        if (this)
        {
            GetComponent<SpriteRenderer>().sprite = symbol.Image();
        }
    }
}
