using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu_EndStars : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI starsText;

    void Start()
    {
        SaveState state = SaveManager.instance.saveState;
        int starValue = state.L1_StarValue + state.L2_StarValue + state.L3_StarValue + state.L4_StarValue;
        starsText.text = "x " + starValue.ToString("00") + "/12";
    }
}
