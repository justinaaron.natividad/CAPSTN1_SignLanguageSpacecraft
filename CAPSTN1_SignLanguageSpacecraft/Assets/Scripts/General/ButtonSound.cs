using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour
{
    [SerializeField] private AudioClip click;
    public void playSound()
    {
        AudioManager.instance.ui.PlayOneShot(click);
    }
}
