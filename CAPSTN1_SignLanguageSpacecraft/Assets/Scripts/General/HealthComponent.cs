using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    public delegate void DeathDelegate();
    public DeathDelegate OnDeath;

    public delegate void HealthChangeDelegate(int currentHP);
    public HealthChangeDelegate OnHealthChanged;

    [Header("Parameters")]
    [SerializeField] private int maxHP;

    private int currentHP;
    private int starRating = 3;

    private void Awake()
    {
        currentHP = maxHP;
    }

    public void TakeDamage()
    {
        currentHP--;
        if(OnHealthChanged != null) OnHealthChanged(currentHP);
        if (currentHP <= 0 && OnDeath != null) OnDeath();
        CheckRating();
    }

    public void Heal()
    {
        currentHP = Mathf.Clamp(currentHP + 1, 0, maxHP);
        if (OnHealthChanged != null) OnHealthChanged(currentHP);
        CheckRating();
    }

    private void CheckRating()
    {
        if (currentHP == maxHP) starRating = 3;
        else if (currentHP == 1) starRating = 1;
        else starRating = 2;
    }

    public float GetHealthPercentage() { return (float)currentHP / (float)maxHP; }

    public int GetStarRating() { return starRating; }

}
