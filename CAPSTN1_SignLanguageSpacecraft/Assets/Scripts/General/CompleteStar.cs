using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompleteStar : MonoBehaviour
{
    public int rating;
    public Sprite star;
    public Sprite dot;
    public List<Image> stars;
    public AudioClip starSound;

    IEnumerator StartAnimation()
    {
        int starIndex = 0;
        bool isStar;

        while (starIndex < 3)
        {
            isStar = rating > starIndex;
            if (isStar) AudioManager.instance.ui.PlayOneShot(starSound);
            stars[starIndex].sprite = isStar ? star : dot;

            stars[starIndex].rectTransform.localScale = Vector3.one * 1.5f;
            LeanTween.scale(stars[starIndex].rectTransform, Vector3.one, 0.5f);
            yield return new WaitForSeconds(0.5f);
            stars[starIndex].GetComponent<RectTransform>().localScale = Vector3.one;
            starIndex++;
            yield return null;

            /*while(scale > 1)
            {
                stars[starIndex].sprite = starIndex < rating ? star : dot;
                stars[starIndex].GetComponent<RectTransform>().localScale = Vector3.one * scale;
                scale -= Time.deltaTime;
                yield return null;
            }*/
            //scale = 1.5f;
        }
    }
}
