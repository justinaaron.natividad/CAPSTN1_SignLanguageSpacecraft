using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreGameUI : MonoBehaviour
{
    public delegate void TutorialFinishDelegate();
    public static TutorialFinishDelegate OnTutorialFinish;

    [Header("UI Panels")]
    [SerializeField] private GameObject dialoguePanel;
    [SerializeField] private GameObject tutorialPanel;
    [SerializeField] private GameObject winPanel;
    [SerializeField] private GameObject losePanel;

    private void OnEnable()
    {
        DialogueManager.DialogueFinished += StartTutorial;
    }

    private void OnDisable()
    {
        DialogueManager.DialogueFinished -= StartTutorial;
    }

    public void SetPanel(Panels panelName)
    {
        dialoguePanel.SetActive(panelName == Panels.Dialogue);
        tutorialPanel.SetActive(panelName == Panels.Tutorial);
        winPanel.SetActive(panelName == Panels.Win);
        losePanel.SetActive(panelName == Panels.Lose);
    }

    public void DisablePanels()
    {
        dialoguePanel.SetActive(false);
        tutorialPanel.SetActive(false);
        winPanel.SetActive(false);
        losePanel.SetActive(false);
    }

    public void StartTutorial()
    {
        SetPanel(Panels.Tutorial);
    }

    public void EndTutorial()
    {
        OnTutorialFinish();
    }
}
