using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioSource bg;
    public AudioSource ui;
    public AudioSource environment;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(instance.gameObject);

        if (PlayerPrefs.HasKey("BGAudio")) bg.volume = PlayerPrefs.GetFloat("BGAudio");
        else bg.volume = 1.0f;
        if (PlayerPrefs.HasKey("UIAudio")) ui.volume = PlayerPrefs.GetFloat("UIAudio");
        else ui.volume = 1.0f;
        if (PlayerPrefs.HasKey("EnviroAudio")) environment.volume = PlayerPrefs.GetFloat("EnviroAudio");
        else environment.volume = 1.0f;
    }

    public void SaveBGAudio(System.Single value)
    {
        PlayerPrefs.SetFloat("BGAudio", value);
        bg.volume = value;
        Debug.Log("Set BGAudio to value: " + value);
    }

    public void SaveUIAudio(System.Single value)
    {
        PlayerPrefs.SetFloat("UIAudio", value);
        ui.volume = value;
        Debug.Log("Set UIAudio to value: " + value);
    }

    public void SaveEnviroAudio(System.Single value)
    {
        PlayerPrefs.SetFloat("EnviroAudio", value);
        environment.volume = value;
        Debug.Log("Set EnviroAudio to value: " + value);
    }
}
