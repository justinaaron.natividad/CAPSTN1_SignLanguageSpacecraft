﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCameraScale : MonoBehaviour
{
    Camera c;
    [SerializeField] float widthRatio = 5.0625f; // default 5.0625;

    void Awake()
    {
        c = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        c.orthographicSize = widthRatio / c.aspect;
    }
}
