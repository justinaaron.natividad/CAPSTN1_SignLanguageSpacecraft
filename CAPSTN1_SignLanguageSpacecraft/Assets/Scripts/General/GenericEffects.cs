using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericEffects : MonoBehaviour
{
    static Color invis = new Color(0f, 0f, 0f, 0f);

    public static void Flash(SpriteRenderer s, Color flashColor, float flashTime)
    {
        s.color = flashColor;
        LeanTween.color(s.gameObject, invis, flashTime);
    }

    public static void Flash(Image i, Color flashColor, float flashTime)
    {
        i.color = flashColor;
        LeanTween.color(i.rectTransform, invis, flashTime);
    }

    public static IEnumerator Shake(Transform t, float shakeMagnitude, float shakeTime)
    {
        Vector2 currentPos = t.position;
        Vector2 shake;
        float shakeCD = shakeTime;

        while (shakeCD > 0)
        {
            shake.x = Random.Range(-shakeMagnitude, shakeMagnitude);
            shake.y = Random.Range(-shakeMagnitude, shakeMagnitude);
            t.position = currentPos + shake;
            shakeCD -= Time.deltaTime;
            yield return null;
        }

        t.position = currentPos;
    }

    public static IEnumerator Shake(RectTransform t, float shakeMagnitude, float shakeTime)
    {
        Vector2 currentPos = t.anchoredPosition;
        Vector2 shake;
        float shakeCD = shakeTime;

        while (shakeCD > 0)
        {
            shake.x = Random.Range(-shakeMagnitude, shakeMagnitude);
            shake.y = Random.Range(-shakeMagnitude, shakeMagnitude);
            t.anchoredPosition = currentPos + shake;
            shakeCD -= Time.deltaTime;
            yield return null;
        }

        t.anchoredPosition = currentPos;
    }

    public static void Pop(Transform t, float popScale, float popTime)
    {
        t.localScale = Vector3.one * popScale;
        LeanTween.scale(t.gameObject, Vector3.one, popTime);
    }

    public static void Pop(RectTransform t, float popScale, float popTime)
    {
        t.localScale = Vector3.one * popScale;
        LeanTween.scale(t.gameObject, Vector3.one, popTime);
    }
}
