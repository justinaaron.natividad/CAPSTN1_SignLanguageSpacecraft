using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutroCover : MonoBehaviour
{
    [SerializeField] private Image coverL;
    [SerializeField] private Image coverR;
    [SerializeField] private float coverDistance;
    [SerializeField] private float coverStart;

    private void Awake()
    {
        StartCoroutine(Uncover(0.5f));
    }
    
    public IEnumerator Uncover(float time)
    {
        float ot = 0;
        yield return new WaitForSeconds(0.1f);
        while (ot < time)
        {
            ot += Time.deltaTime;
            coverL.rectTransform.anchoredPosition = Vector2.Lerp(new Vector2(-coverStart,0), new Vector2(-coverDistance, 0), ot / time);
            coverR.rectTransform.anchoredPosition = Vector2.Lerp(new Vector2(coverStart, 0), new Vector2(coverDistance, 0), ot / time);
            yield return null;
        }
    }

    public IEnumerator Cover(float time)
    {
        float ot = time;
        while (ot > 0)
        {
            ot -= Time.deltaTime;
            coverL.rectTransform.anchoredPosition = Vector2.Lerp(new Vector2(-coverStart, 0), new Vector2(-coverDistance, 0), ot / time);
            coverR.rectTransform.anchoredPosition = Vector2.Lerp(new Vector2(coverStart, 0), new Vector2(coverDistance, 0), ot / time);
            yield return null;
        }
    }
}
