using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM1_N_Node : MonoBehaviour
{
    [Header("Node references")]
    public GameObject parentNode;
    public List<GameObject> childNodes;

    [Header("Node stats")]
    public Color collectedColor;
    public int value;
    public bool collected;

    [Header("Symbol")]
    public Symbol symbol;
    public char letterID;
    public SpriteRenderer symbolSprite;
    public SpriteRenderer nodeSprite;

    [Header("Sprite Materials")]
    public Material defaultMat;
    public Material outlineMat;

    [Header("Line Renderer")]
    public LineRenderer line;
    public Color lineColor;

    private void Awake()
    {
        line = GetComponent<LineRenderer>();
        line.startColor = lineColor;
        line.endColor = lineColor;
        SetCollected(false);
    }

    public bool CompareID(char id)
    {
        return letterID == id;
    }

    #region Setters
    public void SetParentNode(GameObject pNode)
    {
        parentNode = pNode;
        line.positionCount = 2;
        line.SetPosition(0, transform.position);
        line.SetPosition(1, parentNode.transform.position);
    }

    public void SetSymbol(Symbol s)
    {
        symbol = s;
        symbolSprite.sprite = symbol.Image();
        letterID = symbol.ID();
    }

    public void SetCollected(bool c)
    {
        collected = c;
        symbolSprite.gameObject.SetActive(!c);
        nodeSprite.gameObject.SetActive(c);
        nodeSprite.color = c ? collectedColor : Color.white;
    }

    public void SetOutline(bool isOutlined)
    {
        symbolSprite.material = isOutlined ? outlineMat : defaultMat;
    }
    #endregion
}
