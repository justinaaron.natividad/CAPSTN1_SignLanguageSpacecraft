using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM1_N_Background : MonoBehaviour
{
    public GM1_N_Player player;

    [Header("Parameters")]
    public float parallaxMagnitude;
    public float shakeMagnitude;
    public float shakeTime;

    private RectTransform t;
    private Vector2 startPos;
    private Vector2 currentPos;

    #region Delegate Handlers
    private void OnEnable()
    {
        player.health.OnHealthChanged += Shake;
    }

    private void OnDisable()
    {
        player.health.OnHealthChanged -= Shake;
    }
    #endregion

    private void Awake()
    {
        t = GetComponent<RectTransform>();
        startPos = t.anchoredPosition;
    }

    private void FixedUpdate()
    {
        currentPos = startPos - new Vector2(player.transform.position.x, player.transform.position.y) * parallaxMagnitude;
        t.anchoredPosition = currentPos;
    }

    public void Shake(int currentHP)
    {
        StartCoroutine(GenericEffects.Shake(t,shakeMagnitude,shakeTime));
    }
}
