using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM1_N_Player : MonoBehaviour
{
    public delegate void NewLetterDelegate(int index, char letterID);
    public NewLetterDelegate OnNewLetter;

    public delegate void WinDelegate(int starRating);
    public WinDelegate OnWin;

    [Header("Components")]
    public HealthComponent health;
    [SerializeField] private CircleCollider2D collision;
    [SerializeField] private Rigidbody2D playerRB;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private Animator playerAnim;
    [SerializeField] private Joystick joystick;
    [SerializeField] private Image flash;
    [SerializeField] private GM1_N_Map map;

    [Header("Audio Clips")]
    [SerializeField] private AudioClip pickupSound;
    [SerializeField] private AudioClip damageSound;

    [Header("Player Parameters")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotationLerpValue;

    [Header("Extra Options")]
    [SerializeField] private int requiredLetterCount;
    [SerializeField] private bool shouldHighlightLetter;
    [SerializeField] private bool shouldFlashOnCorrect;

    [Header("Private Player Stats")]
    private int lettersCollected;
    private char currentLetterID;

    [Header("Private Player States")]
    private bool isMoving;
    private bool isEnabled;
    private bool canCollide = true;

    [Header("Alphabet Handlers")]
    private string alphabet = "abcdefghijklmnopqrstuvwxyz";
    private int alphabetIndex = 0;

    private void FixedUpdate()
    {
        if (!isEnabled) { return; }

        isMoving = joystick.GetMovementValue() != Vector2.zero;

        playerAnim.SetBool("isMoving", isMoving);
        if (isMoving) { MovePlayer(); }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Collectible") && canCollide)
        {
            GM1_N_Node node = collision.gameObject.GetComponent<GM1_N_Node>();
            if (node == null || node.collected) return;
            CheckCollectedNode(node);
        }
    }

    private void MovePlayer()
    {
        playerRB.velocity = joystick.GetMovementValue() * moveSpeed;
        transform.up = Vector2.Lerp(transform.up, joystick.GetMovementValue(), rotationLerpValue);
    }

    public void GetNewLetter()
    {
        if (alphabetIndex >= 26) return;

        currentLetterID = alphabet[alphabetIndex];
        alphabetIndex++;
        OnNewLetter(alphabetIndex, currentLetterID);

        if (shouldHighlightLetter) map.HighlightNode(currentLetterID);
    }

    private void CheckCollectedNode(GM1_N_Node node)
    {
        bool isCorrect = node.CompareID(currentLetterID);
        if (isCorrect)
            CollectNode(node);
        else
            TakeDamage();
    }

    private void CollectNode(GM1_N_Node node)
    {
        lettersCollected++;
        node.SetCollected(true);
        AudioManager.instance.environment.PlayOneShot(pickupSound);
        GetNewLetter();
        CheckWinCondition();

        if(shouldFlashOnCorrect) GenericEffects.Flash(flash, Color.white, 0.3f);
    }

    private void TakeDamage()
    {
        health.TakeDamage();
        AudioManager.instance.environment.PlayOneShot(damageSound);
        GenericEffects.Flash(flash, Color.red, 0.3f);
        StartCoroutine(StartIFrame(1f));
    }

    private IEnumerator StartIFrame(float inviTime)
    {
        canCollide = false;
        for(int i = 0; i < 5; i++)
        {
            sprite.enabled = false;
            yield return new WaitForSeconds(inviTime / 10);
            sprite.enabled = true;
            yield return new WaitForSeconds(inviTime / 10);
        }
        sprite.enabled = true;
        canCollide = true;
    }

    private void CheckWinCondition()
    {
        if (lettersCollected >= requiredLetterCount)
            OnWin(health.GetStarRating());
    }

    public void SetEnabled(bool e) { isEnabled = e; }
}
