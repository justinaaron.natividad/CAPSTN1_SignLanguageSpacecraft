using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM1_N_Map : MonoBehaviour
{
    [Header("Prefab")]
    public GameObject nodePrefab;   

    [Header("Spawner Parameters")]
    public Vector2 mapSize;
    public int spawnCount;

    [Header("Poisson-Disc Parameters")]
    public int samples;         // Samples for Poisson-Disc Sampling
    public float minDistance;   // Minimum distance between nodes
    public float extraDistance; // Maximum distance between nodes

    public List<GM1_N_Node> nodes = new List<GM1_N_Node>();        // Node list
    private List<GM1_N_Node> spawnNodes = new List<GM1_N_Node>();   // Extra node list for Poisson-Disc Sampling
    private int currentNodeCount = 0;                               // Node count for spawning

    #region Map Generation Methods
    public void GenerateMapAtRandomLocation(int nodeCount)
    {
        float x = Random.Range(-mapSize.x / 2, mapSize.x / 2);
        float y = Random.Range(-mapSize.y / 2, mapSize.y / 2);
        GenerateMapAtLocation(x, y, nodeCount);
    }

    public void GenerateMapAtLocation(float x, float y, int nodeCount)
    {
        ResetMap();
        spawnCount = nodeCount;
        GM1_N_Node startNode = SpawnNode(new Vector2(x, y), null);
        startNode.SetCollected(true);
        while (currentNodeCount < spawnCount && spawnNodes.Count > 0) Poisson();
    }

    public void ResetMap()
    {
        currentNodeCount = 0;

        if (nodes != null)
            for (int i = 0; i < nodes.Count; i++) Destroy(nodes[i].gameObject);

        nodes.Clear();
        spawnNodes.Clear();
    }
    #endregion

    #region Poisson-Disc Methods
    void Poisson()
    {
        int index = Random.Range(0, spawnNodes.Count);
        Vector2 spawnPoint = spawnNodes[index].gameObject.transform.position;
        bool candidateAccepted = false;

        // Get random point around spawn point
        for(int i = 0; i < samples; i++)
        {
            float angle = Random.value * Mathf.PI * 2;  // random angle
            Vector2 dir = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));  // Direction from angle

            // Check if point is valid
            Vector2 point = spawnPoint + dir * Random.Range(2 * minDistance, 2 * minDistance + extraDistance);
            if (IsValid(point))
            {
                SpawnNode(point, spawnNodes[index].gameObject);
                candidateAccepted = true;
                break;
            }
        }

        // Make spawn point invalid
        if (!candidateAccepted) spawnNodes.RemoveAt(index);
    }

    bool IsValid(Vector2 point)
    {
        // Map constraints
        if (point.x < -mapSize.x / 2 || point.x > mapSize.x / 2) return false;
        if (point.y < -mapSize.y / 2 || point.y > mapSize.x / 2) return false;
        if (Vector2.Distance(Vector2.zero, point) < 2 * minDistance)   return false;

        // Node constraints
        foreach (GM1_N_Node n in nodes)
            if (Vector2.Distance(n.gameObject.transform.position, point) < 2 * minDistance) return false;

        return true;
    }

    GM1_N_Node SpawnNode(Vector2 spawnPos, GameObject pNode)
    {
        // Instantiate
        GM1_N_Node node = Instantiate(nodePrefab, spawnPos, Quaternion.identity).GetComponent<GM1_N_Node>();

        if (pNode != null)
        {
            node.SetParentNode(pNode);                                          // Set parent node
            pNode.GetComponent<GM1_N_Node>().childNodes.Add(node.gameObject);   // Set child node
            node.value = pNode.GetComponent<GM1_N_Node>().value + 1;            // Set node value
        }
        else
        {
            node.value = 0;
        }

        nodes.Add(node);        // Add to list
        spawnNodes.Add(node);   // Add to spawn point
        currentNodeCount++;     // Add to count
        return node;
    }
    #endregion

    public void HighlightNode(char letterID)
    {
        foreach(GM1_N_Node n in nodes)
        {
            if (n.symbol == null) continue;
            n.SetOutline(n.symbol.CompareID(letterID));
        }
    }
}
