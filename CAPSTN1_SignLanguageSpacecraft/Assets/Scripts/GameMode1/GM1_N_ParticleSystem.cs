using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM1_N_ParticleSystem : MonoBehaviour
{
    [SerializeField] GameObject particlePrefab;
    [SerializeField] public bool isEmitting = false;
    [SerializeField] float emissionCooldown;

    private void Start()
    {
        StartCoroutine(StartEmitting());
    }

    public IEnumerator StartEmitting()
    {
        isEmitting = true;

        while (isEmitting)
        {
            Instantiate(particlePrefab, transform.position, transform.rotation);
            yield return new WaitForSeconds(emissionCooldown);
        }
    }

    public void StopEmitting() { isEmitting = false; }
}
