using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM1_N_Camera : MonoBehaviour
{
    [SerializeField] private Transform player;

    [Header("Camera Parameters")]
    [SerializeField] private Camera cam;
    [SerializeField] private float zoomOutValue;
    
    private Vector3 cameraPos;
    private Vector3 toPosition = new Vector3(0, 0, -10);
    private float cameraSize;
    private bool isFollowingPlayer = true;

    private void Awake()
    {
        cameraSize = cam.orthographicSize;
    }

    void FixedUpdate()
    {
        if (isFollowingPlayer) MoveToPosition(player.position);
        else MoveToPosition(toPosition);
    }

    public void MoveToPosition(Vector3 pos)
    {
        cameraPos = Vector2.Lerp(transform.position, pos, 0.1f);
        cameraPos.z = -10;
        transform.position = cameraPos;
    }

    public void ZoomOut(float zoomOutTime)
    {
        isFollowingPlayer = false;
        LeanTween.value(cam.gameObject, cam.orthographicSize, cameraSize + zoomOutValue, zoomOutTime).setEase(LeanTweenType.easeOutQuint).setOnUpdate((float flt) => {
            cam.orthographicSize = flt;
        });
        toPosition = new Vector3(0, 0, -10);
    }

    public void ZoomIn(float zoomInTime)
    {
        isFollowingPlayer = false;
        LeanTween.value(cam.gameObject, cam.orthographicSize, cameraSize, zoomInTime).setEase(LeanTweenType.easeOutQuint).setOnUpdate((float flt) => {
            cam.orthographicSize = flt;
        });
        toPosition = player.position;
    }

    public void FollowPlayer()
    {
        isFollowingPlayer = true;
        cam.orthographicSize = cameraSize;
    }
}
