using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM1_N_Manager : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private GM1_N_Camera mainCamera;
    [SerializeField] private GM1_N_Player player;
    [SerializeField] private CompleteStar stars;
    [SerializeField] private Joystick joystick;
    [SerializeField] private GM1_N_Map map;
    [SerializeField] private GM1_N_UI ui;
    [SerializeField] private PreGameUI preGameUI;

    [Header("Parameters")]
    [SerializeField] private float showMapTimer;
    [SerializeField] private float hideMapTimer;

    [Header("Map Strings")]
    [SerializeField] private List<string> mapStrings;

    [Header("Private Map Generation Handlers")]
    private int mapIndex = 0;
    private List<int> mapCheckPoints = new List<int>();
    private List<Symbol> symbolList = new List<Symbol>();

    [Header("Dialogue")]
    [SerializeField] private DialogueTrigger dt;
    [SerializeField] private OutroCover outroCover;
    [SerializeField] private AudioClip bgm;

    private void OnEnable()
    {
        player.OnNewLetter += UpdateMap;
        player.health.OnDeath += OnLose;
        player.OnWin += OnWin;
        PreGameUI.OnTutorialFinish += StartGame;
    }

    private void OnDisable()
    {
        player.OnNewLetter -= UpdateMap;
        player.health.OnDeath -= OnLose;
        player.OnWin -= OnWin;
        PreGameUI.OnTutorialFinish -= StartGame;
    }

    private void Awake()
    {
        mapCheckPoints.Add(mapStrings[0].Length);
        for (int i = 1; i < mapStrings.Count; i++)
            mapCheckPoints.Add(mapCheckPoints[i - 1] + mapStrings[i].Length);
    }

    private void Start()
    {
        AudioManager.instance.bg.clip = bgm;
        AudioManager.instance.bg.Play();
        GenerateNewMap();
        player.GetNewLetter();
        dt.StartDisplay();
        Pause(true);
    }

    public void UpdateMap(int index, char letterID)
    {
        if (index > mapCheckPoints[mapIndex])
        {
            mapIndex++;
            player.health.Heal();
            GenerateNewMap();
            StartCoroutine(RevealMap());
        }
        RemoveSymbol(letterID);
    }

    public IEnumerator RevealMap()
    {
        Pause(true);
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        mainCamera.ZoomOut(showMapTimer);
        yield return new WaitForSeconds(showMapTimer);
        mainCamera.ZoomIn(hideMapTimer);
        yield return new WaitForSeconds(hideMapTimer);

        Pause(false);
        mainCamera.FollowPlayer();
    }

    public void GenerateNewMap()
    {
        // Map generation
        symbolList.Clear();
        symbolList = SymbolManager.instance.GetRandomSetFromString(mapStrings[mapIndex]);
        map.GenerateMapAtLocation(player.transform.position.x, player.transform.position.y, mapStrings[mapIndex].Length + 1);
        // Assign letters to map
        for (int i = 0; i < mapStrings[mapIndex].Length; i++)
            map.nodes[i+1].SetSymbol(symbolList[i]);
    }

    private void StartGame()
    {
        Pause(false);
        preGameUI.DisablePanels();
    }

    private void OnLose()
    {
        Pause(true);
        preGameUI.SetPanel(Panels.Lose);
    }

    private void OnWin(int starRating)
    {
        stars.rating = starRating;
        Pause(true);
        preGameUI.SetPanel(Panels.Win);
        stars.StartCoroutine("StartAnimation");
    }

    public void Pause(bool isPaused)
    {
        joystick.SetEnabled(!isPaused);
        player.GetComponent<GM1_N_Player>().SetEnabled(!isPaused);
    }

    public void RemoveSymbol(char id)
    {
        if (symbolList.Count <= 0) return;
        int index = 0;
        for(int i = 0; i < symbolList.Count; i++)
        {
            if (symbolList[i].CompareID(id)) index = i; 
        }
        symbolList.RemoveAt(index);
    }

    public void LoadScene(string gamemodeName)
    {
        StartCoroutine(LoadOutro(gamemodeName));
    }

    IEnumerator LoadOutro(string gamemodeName)
    {
        StartCoroutine(outroCover.Cover(0.5f));
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(gamemodeName);
    }

    public void OnComplete()
    {
        if (SaveManager.instance.saveState.lastCompleted < 1) SaveManager.instance.saveState.lastCompleted = 1;
        if (SaveManager.instance.saveState.L1_StarValue < player.health.GetStarRating()) SaveManager.instance.saveState.L1_StarValue = player.health.GetStarRating();
        SaveManager.instance.Save();
        LoadScene("MapScene");
    }
}
