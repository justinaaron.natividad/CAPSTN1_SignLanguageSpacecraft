using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joystick : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private CircleCollider2D circleCollider;

    [Header("Joystick Parameters")]
    [SerializeField] private AudioClip moveSound;
    [SerializeField] private float joystickMaxValue;
    [SerializeField] private float joystickDeadZone;

    private Vector2 startPosition;
    private Vector2 movementValue;
    private Vector2 joystickMagnitude;
    private Touch touch;
    private bool activated = false;
    private bool isEnabled = false;
    private bool playedSound = false;

    void Awake()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        if (!isEnabled) return;

        if(Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
                DragStart();
            else if (touch.phase == TouchPhase.Moved)
                Dragging();
            else if (touch.phase == TouchPhase.Ended)
                DragEnd();
        }
    }

    void DragStart()
    {
        if (circleCollider == Physics2D.OverlapPoint(touch.position))
            activated = true;
        else
            activated = false;
    }

    void Dragging()
    {
        if (!activated) return;
        joystickMagnitude = Vector2.ClampMagnitude(touch.position - startPosition, joystickMaxValue);

        if (joystickMagnitude.magnitude >= joystickDeadZone)
        {
            if (!playedSound) { AudioManager.instance.environment.PlayOneShot(moveSound); playedSound = true; }
            movementValue = joystickMagnitude / joystickMaxValue;
        }
        else
        {
            playedSound = false;
            movementValue = Vector2.zero;
        }

        transform.position = startPosition + joystickMagnitude;
    }

    void DragEnd()
    {
        ResetJoystick();
    }

    public Vector2 GetMovementValue()
    {
        return movementValue;
    }

    public void SetEnabled(bool e)
    {
        isEnabled = e;
        if (!e) ResetJoystick();
    }

    void ResetJoystick()
    {
        transform.position = startPosition;
        movementValue = Vector2.zero;
        activated = false;
    }
}
