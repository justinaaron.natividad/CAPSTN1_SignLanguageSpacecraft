using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GM1_N_UI : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private GM1_N_Player player;
    [SerializeField] private Transform playerTransform;
    
    [Header("UI Components")]
    [SerializeField] private List<GameObject> lifeUI;
    [SerializeField] private TextMeshProUGUI xCoord;
    [SerializeField] private TextMeshProUGUI yCoord;
    [SerializeField] private TextMeshProUGUI currentLetter;
    [SerializeField] private Image symbolImage;

    [Header("Parameters")]
    [SerializeField] private float popScale;
    [SerializeField] private float popTime;

    private int playerX;
    private int playerY;

    private void OnEnable()
    {
        player.OnNewLetter += SetCurrentLetter;
        player.health.OnHealthChanged += SetLife;
    }

    private void OnDisable()
    {
        player.OnNewLetter -= SetCurrentLetter;
        player.health.OnHealthChanged -= SetLife;
    }

    void Update()
    {
        playerX = (int)(playerTransform.position.x * 100);
        playerY = (int)(playerTransform.position.y * 100);
        xCoord.text = "X: " + playerX;
        yCoord.text = "Y: " + playerY;
    }

    public void SetLife(int life)
    {
        for(int i = 0; i < lifeUI.Count; i++)
            lifeUI[i].SetActive(i <= life - 1);
    }

    public void SetCurrentLetter(int index, char letterID)
    {
        currentLetter.text = letterID.ToString().ToUpper();
        symbolImage.sprite = SymbolManager.instance.GetLetter(letterID).Image();
        symbolImage.color = Color.white;
        GenericEffects.Pop(currentLetter.rectTransform, popScale, popTime);
        GenericEffects.Pop(symbolImage.rectTransform, popScale, popTime);
    }
}
