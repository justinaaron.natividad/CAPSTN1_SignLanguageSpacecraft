using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SymbolManager : MonoBehaviour
{
    public static SymbolManager instance;
    public List<Symbol> lettersList;
    public Dictionary<char, Symbol> lettersDictionary;

    
    private void Awake()
    {
        // Create Singleton
        if (instance != null)
            Destroy(this);
        else
            instance = this;

        // Get all letters from allocated resources folder
        lettersList = new List<Symbol>();
        lettersDictionary = new Dictionary<char, Symbol>();
        foreach (Symbol s in Resources.LoadAll<Symbol>("ScriptableObjects/Letters"))
            lettersDictionary.Add(s.ID(), s);
        ReloadList();
    }
    
    #region Single Letter
    // Retrieves a specific letter from char
    public Symbol GetLetter(char id)
    {
        Symbol s = null;
        lettersDictionary.TryGetValue(id, out s);
        return s;
    }

    // Retrieves a random letter
    public Symbol GetRandomLetter()
    {
        return lettersList[Random.Range(0, lettersList.Count)];
    }
    #endregion

    #region Letter Set
    // Retrieves a random unique set of letters from list
    public List<Symbol> GetRandomSet(int number)
    {
        List<Symbol> retrieved = new List<Symbol>();

        for(int i = 0; i < number; i++)
        {
            int index = Random.Range(0, lettersList.Count);
            retrieved.Add(lettersList[index]);
            lettersList.RemoveAt(index);
        }

        ReloadList();
        return retrieved;
    }

    // Retrieves a set from string to form words and predetermined sets
    public List<Symbol> GetSetFromString(string inputString)
    {
        List<Symbol> retrieved = new List<Symbol>();

        Symbol s;
        foreach(char c in inputString)
        {
            if(lettersDictionary.TryGetValue(c, out s))
            {
                retrieved.Add(s);
            }
        }
        return retrieved;
    }

    public List<Symbol> GetRandomSetFromString(string inputString)
    {
        string randomString = GenerateRandomString(inputString);
        return GetSetFromString(randomString);
    }
    #endregion

    #region Manager/Debug Methods
    // Reloads lettersList (used after taking a random unique set)
    public void ReloadList()
    {
        lettersList.Clear();
        foreach (Symbol s in Resources.LoadAll<Symbol>("ScriptableObjects/Letters"))
        {
            lettersList.Add(s);
        }
    }

    // Print set for debug purposes
    public void PrintSet(List<Symbol> symbolList)
    {
        string printString = "";
        foreach (Symbol s in symbolList)
            printString += s.ID().ToString();
        Debug.Log(printString);
    }
    #endregion

    #region Misc
    // taken from GM3 Manager temporarily
    private string GenerateRandomString(string word)
    {
        List<char> oldWord = new List<char>(word);
        string newWord = "";
        int index;

        for (int i = 0; i < word.Length; i++)
        {
            index = Random.Range(0, oldWord.Count - 1);
            newWord += oldWord[index].ToString();
            oldWord.RemoveAt(index);
        }

        return newWord;
    }
    #endregion
}
