using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Symbol", menuName = "Symbol")]
public class Symbol : ScriptableObject
{
    [SerializeField] private Sprite image;
    [SerializeField] private char id;

    #region Getters
    public char ID()
    {
        return id;
    }

    public Sprite Image()
    {
        return image;
    }
    #endregion

    public bool CompareID(char inputID)
    {
        if (this.id == inputID) return true; else return false;
    }
}
