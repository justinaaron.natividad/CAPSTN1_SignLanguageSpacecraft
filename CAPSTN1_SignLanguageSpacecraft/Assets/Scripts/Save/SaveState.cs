public class SaveState
{
    // Level 1
    public int lastCompleted = 0;
    public int L1_StarValue = 0;
    public int L2_StarValue = 0;
    public int L3_StarValue = 0;
    public int L4_StarValue = 0;

    public string Serialize()
    {
        string toReturn;

        toReturn = lastCompleted.ToString();
        toReturn += L1_StarValue.ToString();
        toReturn += L2_StarValue.ToString();
        toReturn += L3_StarValue.ToString();
        toReturn += L4_StarValue.ToString();
        return toReturn;
    }

    public void DeSerialize(string toDeserialize)
    {
        lastCompleted = int.Parse(toDeserialize[0].ToString());
        L1_StarValue  = int.Parse(toDeserialize[1].ToString());
        L2_StarValue  = int.Parse(toDeserialize[2].ToString());
        L3_StarValue  = int.Parse(toDeserialize[3].ToString());
        L4_StarValue  = int.Parse(toDeserialize[4].ToString());
    }
}
