using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;

    public SaveState saveState;

    private void Awake()
    {
        // Create Singleton
        if (instance != null)
            Destroy(this);
        else
            instance = this;

        DontDestroyOnLoad(instance);
    }

    void Start()
    {
        saveState = new SaveState();
        Load();
    }

    public void Load()
    {
        if (PlayerPrefs.HasKey("save"))
        {
            string save = PlayerPrefs.GetString("save");
            Debug.Log("Loading File: " + save);
            saveState.DeSerialize(save);
        }
        else
        {
            Save();
            Debug.Log("Creating new save file");
        }
    }

    public void Save()
    {
        PlayerPrefs.SetString("save", saveState.Serialize());
        Debug.Log("Saving File: " + saveState.Serialize());
    }
}
