using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM2_N_Manager : MonoBehaviour
{
    public delegate void GM2CompareDelegate(bool isCorrect);
    public static GM2CompareDelegate OnCompare;

    [Header("Components")]
    public HealthComponent health;
    [SerializeField] private GM2_N_UI ui;
    [SerializeField] private PreGameUI preGameUI;
    [SerializeField] private CompleteStar stars;

    public List<GM2_N_Socket> Sockets;
    [SerializeField] private List<GM2_N_Wire> Wires;
    [SerializeField] private GM2_N_Lever lever;

    [Header("Stats")]
    [SerializeField] private int maxSets;
    [SerializeField] OutroCover outroCover;
    [SerializeField] private AudioClip bgm;

    private List<Symbol> Symbols;
    private List<GM2_N_Wire> WireTemp;
    private int currentSet = 0;

    [SerializeField] private DialogueTrigger dt;

    private void OnEnable()
    {
        GM2_N_Lever.OnLeverActivated += CompareIDs;
        PreGameUI.OnTutorialFinish += StartGame;
        health.OnDeath += OnLose;
    }

    private void OnDisable()
    {
        GM2_N_Lever.OnLeverActivated -= CompareIDs;
        PreGameUI.OnTutorialFinish -= StartGame;
        health.OnDeath -= OnLose;
    }

    private void Start()
    {
        AudioManager.instance.bg.clip = bgm;
        AudioManager.instance.bg.Play();
        dt.StartDisplay();
        RefreshSet();
        Pause(true);
    }

    void StartGame()
    {
        Debug.Log("Check");
        Pause(false);
        preGameUI.DisablePanels();
        
    }

    private void OnLose()
    {
        Pause(true);
        preGameUI.SetPanel(Panels.Lose);
    }

    private void OnWin(int starRating)
    {
        stars.rating = starRating;
        Pause(true);
        preGameUI.SetPanel(Panels.Win);
        stars.StartCoroutine("StartAnimation");
    }

    public void Pause(bool isPaused)
    {
        foreach(GM2_N_Wire w in Wires)
        {
            w.SetEnabled(!isPaused);
        }
        lever.SetEnabled(!isPaused);
    }

    

    public void CompareIDs()
    {
        // Check all comparisons
        bool isCorrect = true;
        for (int i = 0; i < 3; i++)
        {
            if (!Wires[i].CompareID())
            {
                GenericEffects.Flash(ui.correctFlash[i], Color.red, 0.5f);
                isCorrect = false;
            }
            else
            {
                GenericEffects.Flash(ui.correctFlash[i], Color.green, 0.5f);
            }
        }
            

        OnCompare(isCorrect);
        if (!isCorrect) health.TakeDamage();
        currentSet++;
        if (currentSet >= maxSets && health.GetHealthPercentage() > 0f) OnWin(health.GetStarRating());
        RefreshSet();
    }

    public void RefreshSet()
    {
        Symbols = SymbolManager.instance.GetRandomSet(3);
        WireTemp = new List<GM2_N_Wire>(Wires);

        for(int i = 0; i < 3; i++)
        {
            int index = Random.Range(0, WireTemp.Count);
            Sockets[i].SetSymbol(Symbols[i]);

            WireTemp[index].SetLetter(Symbols[i].ID());
            WireTemp.RemoveAt(index);
            Wires[i].ResetWire();
        }
    }

    public void LoadScene(string gamemodeName)
    {
        StartCoroutine(LoadOutro(gamemodeName));
    }

    IEnumerator LoadOutro(string gamemodeName)
    {
        StartCoroutine(outroCover.Cover(0.5f));
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(gamemodeName);
    }

    public void OnComplete()
    {
        if (SaveManager.instance.saveState.lastCompleted < 2) SaveManager.instance.saveState.lastCompleted = 2;
        if (SaveManager.instance.saveState.L2_StarValue < health.GetStarRating()) SaveManager.instance.saveState.L2_StarValue = health.GetStarRating();
        SaveManager.instance.Save();
        LoadScene("MapScene");
    }
}
