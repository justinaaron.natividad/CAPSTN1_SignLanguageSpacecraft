using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM2_N_Fuse : MonoBehaviour
{
    [SerializeField] private Image i;
    [SerializeField] private Sprite brokenSprite;

    public void Break() { i.sprite = brokenSprite; }
}
