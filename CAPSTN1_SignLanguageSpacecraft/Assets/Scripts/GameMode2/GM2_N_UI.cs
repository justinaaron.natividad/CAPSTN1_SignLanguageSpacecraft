using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM2_N_UI : MonoBehaviour
{
    [Header("UI Components")]
    [SerializeField] private Image background;
    [SerializeField] private Image incorrectFlash;
    [SerializeField] public List<Image> correctFlash;
    [SerializeField] private List<Image> PointLights;
    [SerializeField] private List<GM2_N_Fuse> fuses;
    [SerializeField] private Color flashColor;
    [SerializeField] private Color correctColor;
    [SerializeField] private Color wrongColor;

    [Header("Sparks")]
    [SerializeField] private GameObject SparkPrefab;
    [SerializeField] private Vector2 sparkArea;
    [SerializeField] private float sparkTime;
    [SerializeField] private float sparkReduction;
    [SerializeField] private List<Transform> sparkPositions;

    [Header("Audio")]
    [SerializeField] private AudioClip correctSound;
    [SerializeField] private AudioClip wrongSound;

    private int pointLightIndex = 0;
    private int fusesIndex = 0;


    //Sales
    //public BGShaker bgShaker;
    //public Flash flashImage;
    //public GameObject[] Sparks;
    //public int sparkCount = 5;
    //private int j = 0;

    private void OnEnable()
    {
        GM2_N_Manager.OnCompare += UpdateUI;
    }

    private void OnDisable()
    {
        GM2_N_Manager.OnCompare -= UpdateUI;
    }

    void UpdateUI(bool isCorrect)
    {
        if(pointLightIndex < PointLights.Count)
                PointLights[pointLightIndex].color = isCorrect ? correctColor : wrongColor;
        pointLightIndex++;

        if(isCorrect)
        {
            AudioManager.instance.environment.PlayOneShot(correctSound);
        }
        else
        {
            if (fusesIndex < fuses.Count)
            {
                fuses[fusesIndex].Break();
                fusesIndex++;
            }
            DamageUI();
            AudioManager.instance.environment.PlayOneShot(wrongSound);
        }
    }

    void DamageUI()
    {
        StartCoroutine(GenericEffects.Shake(background.rectTransform, 20f, 1.0f));
        GenericEffects.Flash(incorrectFlash, flashColor, 0.5f);
        if (IsInvoking("PlaySpark")) CancelInvoke("PlaySpark");
        sparkTime -= sparkReduction;
        InvokeRepeating("PlaySpark", 0.01f, sparkTime);
    }

    void PlaySpark() {
        GameObject spark = Instantiate(SparkPrefab);
        int index = (int)Random.Range(0f, sparkPositions.Count);
        spark.transform.position = sparkPositions[index].position;
    }
}
