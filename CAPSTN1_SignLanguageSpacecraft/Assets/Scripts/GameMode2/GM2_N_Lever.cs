using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GM2_N_Lever : MonoBehaviour
{
    public delegate void ActivateLeverDelegate();
    public static ActivateLeverDelegate OnLeverActivated;
    public GM2_N_Manager manager;

    [Header("Lever Parts")]
    [SerializeField] private Image leverImage;
    [SerializeField] private BoxCollider2D boxCollision;
    [SerializeField] private Transform leverEnd;
    [SerializeField] private TextMeshProUGUI leverText;

    [Header("Lever Materials")]
    public Material defaultMat;
    public Material outlineMat;

    [Header("Audio")]
    [SerializeField] private AudioClip pullDown;

    private float leverY;   
    private float endY;     
    private Touch touch;        
    private Vector3 dragPos;    
    private bool selected;      
    private bool isEnabled;
    private bool isHighlighted;

    void Awake()
    {
        leverY = transform.position.y;  // Original lever position
        endY = leverEnd.position.y;     // Max lever position
    }

    void Update()
    {
        if (!isEnabled) return;

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
                SelectLever();
            else if (touch.phase == TouchPhase.Moved)
                MoveLever();
        }
        else
            RetractLever();

        CheckIfHighlighted();
    }

    void SelectLever()
    {
        dragPos = Camera.main.ScreenToWorldPoint(touch.position);
        dragPos.z = 0;

        if (boxCollision == Physics2D.OverlapPoint(dragPos))
            selected = true;
        else
            selected = false;
    }

    void MoveLever()
    {
        if (!selected) return;
        dragPos = Camera.main.ScreenToWorldPoint(touch.position);
        dragPos.z = 0;

        transform.position = new Vector3(transform.position.x, Mathf.Clamp(dragPos.y, endY, leverY), 0);
    }

    void RetractLever()
    {
        transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, leverY, 0.05f), 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == ("End"))
        {
            AudioManager.instance.environment.PlayOneShot(pullDown);
            OnLeverActivated();
        }  
    }

    public void SetEnabled(bool e) { isEnabled = e; }

    void CheckIfHighlighted()
    {
        isHighlighted = true;
        foreach(GM2_N_Socket s in manager.Sockets)
            if (!s.occupied) isHighlighted = false;

        leverImage.material = isHighlighted ? outlineMat : defaultMat;
        leverText.gameObject.SetActive(isHighlighted);
    }
}
