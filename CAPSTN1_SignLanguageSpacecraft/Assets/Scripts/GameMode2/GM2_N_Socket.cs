using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM2_N_Socket : MonoBehaviour
{
    [Header("Socket Parts")]
    [SerializeField] private Transform attachPoint; // Reference for attach point for wire  
    [SerializeField] private Image symbolImage;     // Image on top of socket

    // Private variables
    private Symbol symbol;                      // Assigned symbol
    public char letterID { get; private set; }  // ID from symbol for direct comparison
    public bool occupied { get; set; }          // Check if socket is occupied by a wire (no need for reference to wire)

    public void SetSymbol(Symbol s)
    {
        symbol = s;
        letterID = symbol.ID();
        symbolImage.sprite = symbol.Image();
        symbolImage.color = new Vector4(255, 255, 255, 255);
    }

    public Vector3 GetAttachPoint()
    {
        return attachPoint.position;
    }
}
