using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM2_N_WireEnd : MonoBehaviour
{
    [SerializeField] private GM2_N_Wire wireBase;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If new socket is unoccupied
        if (!collision.gameObject.GetComponent<GM2_N_Socket>().occupied)
        {
            // Change old socket to unoccupied
            if (wireBase.socket != null)
                wireBase.socket.occupied = false;

            // Change to new socket
            wireBase.socket = collision.gameObject.GetComponent<GM2_N_Socket>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // Remove socket
        if(wireBase.socket == collision.gameObject.GetComponent<GM2_N_Socket>())
        {
            wireBase.socket.occupied = false;
            wireBase.socket = null;
        } 
    }
}
