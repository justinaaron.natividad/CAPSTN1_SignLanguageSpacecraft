using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GM2_N_Wire : MonoBehaviour
{
    [Header("Wire Components")]
    public GameObject WireStart;        // GameObject for LineRenderer and position
    public GameObject WireEnd;          // GameObject for BoxCollider2D and position
    public GameObject Cursor;
    public TextMeshProUGUI LetterText;  // Letter prompt

    [Header("Audio")]
    [SerializeField] private AudioClip pick;
    [SerializeField] private AudioClip attach;

    [Header("Materials")]
    //[SerializeField] private Material normalMat;
    //[SerializeField] private Material outlineMat;

    // Sub-components
    private BoxCollider2D boxCollision;  // Wire end hitbox
    private LineRenderer line;           // Wire from WireStart

    // Letter comparison stuff
    public GM2_N_Socket socket { get; set; }    // Socket wire is attached to
    public char letterID { get; private set; }  // ID for direct comparison

    // Private dragging stuff
    private Touch touch;        // Touch variable
    private Vector3 startPos;   // Wire start position
    private Vector3 dragPos;    // Touch position
    private bool selected;      // To check if object is selected
    private bool isEnabled;
    private bool isAttached;

    private void Awake()
    {
        boxCollision = WireEnd.GetComponent<BoxCollider2D>();
        line = WireStart.GetComponent<LineRenderer>();

        // Store plug starting position
        startPos = WireEnd.transform.position;
        startPos.z = 0;

        // Initialize line
        line.positionCount = 2;
        line.SetPosition(0, new Vector3(WireStart.transform.position.x, WireStart.transform.position.y, -2));
    }

    void Update()
    {
        if (!isEnabled) return;
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
                DragStart();
            else if (touch.phase == TouchPhase.Moved)
                Dragging();
            else if (touch.phase == TouchPhase.Ended)
                DragRelease();
        }

        if (!isAttached && !selected) DisplayCursor(true);
        else DisplayCursor(false);
    }

    #region Drag Methods
    void DragStart()
    {
        // Get touch position
        dragPos = Camera.main.ScreenToWorldPoint(touch.position);
        dragPos.z = 0;

        // Check if plug is selected
        if (boxCollision == Physics2D.OverlapPoint(dragPos))
        {
            AudioManager.instance.environment.PlayOneShot(pick);
            selected = true;
        }
            
        else
            selected = false;
    }

    void Dragging()
    {
        if (!selected) return; // Don't move wire if not selected

        dragPos = Camera.main.ScreenToWorldPoint(touch.position);
        dragPos.z = 0;

        WireEnd.transform.position = dragPos;
        line.SetPosition(1, new Vector3(WireEnd.transform.position.x, WireEnd.transform.position.y, -2));
    }

    void DragRelease()
    {
        if (!selected) return; // Don't reset if wire not selected

        // Plug to socket
        if (socket != null)
        {
            isAttached = true;
            socket.occupied = true;
            WireEnd.transform.position = socket.GetAttachPoint();
            AudioManager.instance.environment.PlayOneShot(attach);
        }
        // Return wire
        else
        {
            isAttached = false;
            WireEnd.transform.position = startPos;
        }

        selected = false;
        line.SetPosition(1, new Vector3(WireEnd.transform.position.x, WireEnd.transform.position.y, -2));
    }
    #endregion

    public void SetLetter(char id)
    {
        letterID = id;
        LetterText.text = letterID.ToString().ToUpper();
    }

    public bool CompareID()
    {
        return socket != null && letterID == socket.letterID;
    }

    public void SetEnabled(bool e)
    {
        isEnabled = e;
    }

    public void DisplayCursor(bool isDisplayed)
    {
        //WireEnd.GetComponent<Image>().material = isDisplayed ? outlineMat : normalMat;
        Cursor.SetActive(isDisplayed);
    }

    public void ResetWire()
    {
        if (socket != null)
        {
            socket.occupied = false;
            socket = null;
        }
        WireEnd.transform.position = startPos;
        selected = false;
        line.SetPosition(1, new Vector3(WireEnd.transform.position.x, WireEnd.transform.position.y, -2));
    }
}
