﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.DeviceSimulation;
using UnityEngine;

public class MapNavigation : MonoBehaviour
{
    // Start is called before the first frame update

    [Header("Manager")]
    public MenuManager manager;

    [Header("Map Constraints")]
    public float Map_Low;
    public float Map_High;
    private RectTransform t;
    private Vector2 Map_startPos;

    [Header("Background Constraints")]
    public RectTransform bg;
    public float BG_Low;
    public float BG_High;
    private Vector2 BG_startPos;

    [Header("Environment Constraints")]
    public RectTransform enviro;
    public float E_Low;
    public float E_High;
    private Vector2 E_startPos;

    [Header("Parameters")]
    public int SequenceID;
    public float Sensitivity;
    public float LerpValue;
    public bool isEnabled;

    private float MapSpeed;
    private Vector3 mouseDownPos;
    private Vector3 mousePos;
    
    private void Awake()
    {
        t = GetComponent<RectTransform>();
        isEnabled = true;
    }

    void Update()
    {
        if (manager.Sequence == SequenceID)
        {
            MoveMap();
            // Constraints
            enviro.anchoredPosition = Vector2.Lerp(enviro.anchoredPosition, new Vector2(0, Mathf.Clamp(enviro.anchoredPosition.y, E_Low, E_High)), 0.5f);
            bg.anchoredPosition = Vector2.Lerp(bg.anchoredPosition, new Vector2(0, Mathf.Clamp(bg.anchoredPosition.y, BG_Low, BG_High)), 0.5f);
            t.anchoredPosition = Vector2.Lerp(t.anchoredPosition, new Vector2(0, Mathf.Clamp(t.anchoredPosition.y, Map_Low, Map_High)), 0.5f);
        }
    }

    private void MoveMap()
    {
        if (Input.touchCount > 0 && isEnabled)
        {
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Began)
            {
                Map_startPos = t.anchoredPosition;
                BG_startPos = bg.anchoredPosition;
                E_startPos = enviro.anchoredPosition;
                mouseDownPos = touch.position;
            }
            else if(touch.phase == TouchPhase.Moved)
            {
                mousePos = touch.position;
                MapSpeed = (mousePos.y - mouseDownPos.y) * Sensitivity;
                t.anchoredPosition = Vector2.Lerp(t.anchoredPosition, Map_startPos + new Vector2(0, MapSpeed), LerpValue);
                enviro.anchoredPosition = Vector2.Lerp(enviro.anchoredPosition, E_startPos + new Vector2(0, MapSpeed / 10), LerpValue);
                bg.anchoredPosition = Vector2.Lerp(bg.anchoredPosition, BG_startPos + new Vector2(0, MapSpeed / 20), LerpValue);
            }
        }
    }
}
