using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu_Transition : MonoBehaviour
{
    [Header("Manager")]
    public MenuManager manager;

    [Header("Parameters")]
    public float LerpValue;
    public float SequenceID;

    [Header("Positions")]
    [SerializeField] public Vector3 AwakePosition;
    private Vector3 StartPosition;
    private RectTransform t;

    private void Awake()
    {
        t = GetComponent<RectTransform>();
        StartPosition = t.anchoredPosition;
        t.anchoredPosition = AwakePosition;
    }

    void FixedUpdate()
    {
        if (manager.Sequence == SequenceID)
        {
            t.anchoredPosition = Vector3.Lerp(t.anchoredPosition, StartPosition, LerpValue);
        }
        else if (manager.Sequence == SequenceID + 1)
        {
            t.anchoredPosition = StartPosition;
        }
    }
}
