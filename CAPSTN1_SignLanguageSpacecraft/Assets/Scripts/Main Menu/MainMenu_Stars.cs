using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu_Stars : MonoBehaviour
{
    public List<Image> stars;
    public int level;
    public int starValue = 0;

    void Start()
    {
        switch (level)
        {
            case 1: starValue = SaveManager.instance.saveState.L1_StarValue; break;
            case 2: starValue = SaveManager.instance.saveState.L2_StarValue; break;
            case 3: starValue = SaveManager.instance.saveState.L3_StarValue; break;
            case 4: starValue = SaveManager.instance.saveState.L4_StarValue; break;
            default: break;
        }

        for(int i = 0; i < starValue; i++)
        {
            stars[i].color = Color.white;
        }
    }
}
