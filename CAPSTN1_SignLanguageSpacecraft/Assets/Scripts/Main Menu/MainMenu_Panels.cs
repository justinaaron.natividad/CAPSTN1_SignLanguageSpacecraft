using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu_Panels : MonoBehaviour
{
    public MapNavigation map;

    public GameObject fade;
    public GameObject fadeButton;
    public List<GameObject> Panels;

    public void SetPanel(int panelNumber)
    {
        fade.SetActive(true);
        fadeButton.SetActive(true);
        map.isEnabled = false;

        for(int i = 0; i < Panels.Count; i++)
        {
            Panels[i].SetActive(panelNumber == i);
        }
    }

    public void DisablePanels()
    {
        fade.SetActive(false);
        fadeButton.SetActive(false);
        map.isEnabled = true;

        for (int i = 0; i < Panels.Count; i++)
        {
            Panels[i].SetActive(false);
        }
    }
}
