using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [Header("Sequence Parameters")]
    public int Sequence = 0;
    public List<float> transitionTimes;
    private float transitionCD;
    public OutroCover outroCover;
    public AudioClip bgm;

    [Header("Ending")]
    public MapNavigation map;
    public DialogueTrigger dt;
    public GameObject DialoguePanel;
    public GameObject EndPanel;

    private void OnEnable()
    {
        DialogueManager.DialogueFinished += StartPanel;
    }

    private void OnDisable()
    {
        DialogueManager.DialogueFinished -= StartPanel;
    }

    public void StartDialogue()
    {
        map.isEnabled = false;
        dt.StartDisplay();
    }

    public void StartPanel()
    {
        map.isEnabled = false;
        EndPanel.SetActive(true);
    }

    public void RemovePanel()
    {
        map.isEnabled = true;
        EndPanel.SetActive(false);
    }

    private void Awake()
    {
        transitionCD = transitionTimes[0];
    }

    private void Start()
    {
        if(AudioManager.instance.bg.clip != bgm)
        {
            AudioManager.instance.bg.clip = bgm;
            AudioManager.instance.bg.Play();
        }
    }

    private void Update()
    {
        if(Sequence < transitionTimes.Count)
        {
            transitionCD -= Time.deltaTime;

            if (transitionCD <= 0)
            {
                Sequence++;
                if (Sequence < transitionTimes.Count) transitionCD = transitionTimes[Sequence];
            }
        }
    }

    public void LoadScene(string gamemodeName)
    {
        StartCoroutine(LoadOutro(gamemodeName));
    }

    IEnumerator LoadOutro(string gamemodeName)
    {
        StartCoroutine(outroCover.Cover(0.5f));
        yield return new WaitForSeconds(0.5f);
        if (gamemodeName != "TitleScene") AudioManager.instance.bg.Stop();
        SceneManager.LoadScene(gamemodeName);
    }
}
