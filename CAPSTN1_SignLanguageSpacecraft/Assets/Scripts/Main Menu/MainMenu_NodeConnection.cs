using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu_NodeConnection : MonoBehaviour
{
    [Header("Manager")]
    public MenuManager manager;
    public MainMenu_Panels panels;

    [Header("Previous Node")]
    public MainMenu_NodeConnection connection;

    [Header("Node Parameters")]
    public TextMeshProUGUI levelText;
    public int lastLevel;
    public int sequenceStartID;
    public int sequenceEndID;
    public float transitionTime;
    public bool isEnabled;

    [Header("Colors")]
    public Color enabledColor;
    public Color disabledColor;
    public Color lineColor;

    // Private variables
    public bool complete { get; private set; }
    public RectTransform t { get; private set; }
    public LineRenderer line { get; private set; }
    public Image image { get; private set; }

    private float transitionCD = 0;

    void Awake()
    {
        isEnabled = SaveManager.instance.saveState.lastCompleted >= lastLevel;

        t = GetComponent<RectTransform>();
        line = GetComponent<LineRenderer>();
        image = GetComponent<Image>();

        image.color = Color.white * 0;
        levelText.color = Color.white * 0;
        line.positionCount = 2;
        line.startColor = lineColor;
        line.endColor = lineColor;

        if (connection == null) StartCoroutine(Pop());
        else StartCoroutine(DrawLine());
    }

    private void Update()
    {
        if (manager.Sequence != sequenceEndID || connection == null) return;

        Vector3 tPos = new Vector3(t.position.x, t.position.y, 0);
        Vector3 cPos = new Vector3(connection.t.position.x, connection.t.position.y, 0);

        line.SetPosition(0, tPos);
        line.SetPosition(1, cPos);
    }

    IEnumerator DrawLine()
    {
        while (transitionCD < transitionTime)
        {
            if (manager.Sequence >= sequenceStartID && connection.complete)
            {
                Vector3 tPos = new Vector3(t.position.x, t.position.y, 0);
                Vector3 cPos = new Vector3(connection.t.position.x, connection.t.position.y, 0);

                transitionCD = Mathf.Clamp(transitionCD + Time.deltaTime, 0, transitionTime);
                line.SetPosition(0, Vector3.Lerp(cPos, tPos, transitionCD / transitionTime));
                line.SetPosition(1, cPos);
            }
            yield return null;
        }
        StartCoroutine(Pop());
    }

    IEnumerator Pop()
    {
        float scale = 1.5f;
        complete = true;

        while (scale >= 1)
        {
            if(manager.Sequence >= sequenceStartID)
            {
                image.color = isEnabled ? enabledColor : disabledColor;
                levelText.color = isEnabled ? enabledColor : disabledColor;
                t.localScale = Vector3.one * scale;
                scale -= Time.deltaTime;
            }
            yield return null;
        }

        t.localScale = Vector3.one;
    }

    public void EnablePanel(int panelNumber)
    {
        if (isEnabled)
        {
            panels.SetPanel(panelNumber);
        }
    }

    public void EnableEndDialogue()
    {
        if (isEnabled) manager.StartDialogue();
    }
}
