﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class TitleManager : MonoBehaviour
{
    public delegate void TransitionDelegate();
    public static TransitionDelegate OnTransition;

    [Header("Game Scenes")]
    public string gameScene = "MapScene";

    [Header("Images")]
    public Logo logo;
    public Image flash;
    public TextMeshProUGUI startText;
    public Background background;

    [Header("Buttons")]
    public Button buttonBG;
    public Button buttonCredits;
    public Button buttonSettings;

    [Header("Panels")]
    public GameObject normal;
    public GameObject credits;
    public GameObject settings;

    [Header("Sliders")]
    public Slider bgSlider;
    public Slider uiSlider;
    public Slider enviroSlider;

    [SerializeField] private AudioClip pling;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("BGAudio")) bgSlider.value = PlayerPrefs.GetFloat("BGAudio");
        else bgSlider.value = 1.0f;
        if (PlayerPrefs.HasKey("UIAudio")) uiSlider.value = PlayerPrefs.GetFloat("UIAudio");
        else uiSlider.value = 1.0f;
        if (PlayerPrefs.HasKey("EnviroAudio")) enviroSlider.value = PlayerPrefs.GetFloat("EnviroAudio");
        else enviroSlider.value = 1.0f;
    }

    public void LeaveScene()
    {
        SceneManager.LoadScene(gameScene);
    }

    public void OnTapAnywhere()
    {
        GenericEffects.Flash(flash, Color.white, 0.3f);
        buttonBG.gameObject.SetActive(false);
        buttonCredits.gameObject.SetActive(false);
        buttonSettings.gameObject.SetActive(false);
        startText.gameObject.SetActive(false);
        AudioManager.instance.environment.PlayOneShot(pling);
        OnTransition();
    }

    public void SetPanel(int panelNumber)
    {
        normal.SetActive(panelNumber == 0);
        credits.SetActive(panelNumber == 1);
        settings.SetActive(panelNumber == 2);
    }

    public void ResetData()
    {
        Debug.Log("Resetting Data...");
        SaveManager.instance.saveState.lastCompleted = 0;
        SaveManager.instance.saveState.L1_StarValue  = 0;
        SaveManager.instance.saveState.L2_StarValue  = 0;
        SaveManager.instance.saveState.L3_StarValue  = 0;
        SaveManager.instance.saveState.L4_StarValue  = 0;
        SaveManager.instance.Save();
    }

    public void UnlockLevels()
    {
        Debug.Log("Unlocking Levels...");
        SaveManager.instance.saveState.lastCompleted = 4;
        SaveManager.instance.saveState.L1_StarValue  = 3;
        SaveManager.instance.saveState.L2_StarValue  = 3;
        SaveManager.instance.saveState.L3_StarValue  = 3;
        SaveManager.instance.saveState.L4_StarValue  = 3;
        SaveManager.instance.Save();
    }
}
