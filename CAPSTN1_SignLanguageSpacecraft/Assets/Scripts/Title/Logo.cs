﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logo : MonoBehaviour
{
    [SerializeField] private RectTransform rTransform;
    [SerializeField] private float bobSpeed;
    [SerializeField] private float bobDistance;
    [SerializeField] private float leaveTime;

    private Vector3 originalLocation;
    private bool isBobbing = true;
    private float t = 0;

    private void OnEnable()
    {
        TitleManager.OnTransition += Leave;
    }

    private void OnDisable()
    {
        TitleManager.OnTransition -= Leave;
    }

    void Awake()
    {
        originalLocation = rTransform.anchoredPosition;
    }

    void FixedUpdate()
    {
        if (!isBobbing) return;
        t += Time.deltaTime * bobSpeed;
        rTransform.anchoredPosition = originalLocation + new Vector3(0, Mathf.Sin(t) * bobDistance, 0);
    }

    void Leave()
    {
        isBobbing = false;
        LeanTween.moveY(gameObject, 30f, leaveTime).setEase(LeanTweenType.easeInCubic); ;
    }
}
