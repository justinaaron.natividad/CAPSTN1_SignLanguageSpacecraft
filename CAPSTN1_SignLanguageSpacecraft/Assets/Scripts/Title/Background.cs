﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] private TitleManager titleManager;
    [SerializeField] private RectTransform rTransform;
    [SerializeField] private float leaveTime;
    [SerializeField] private float shakeTime;
    [SerializeField] private float shakeStrength;
    [SerializeField] private OutroCover outroCover;

    private void OnEnable()
    {
        TitleManager.OnTransition += DoTransition;
    }

    private void OnDisable()
    {
        TitleManager.OnTransition -= DoTransition;
    }

    void DoTransition()
    {
        StartCoroutine(TransitionIEnum());
    }

    IEnumerator TransitionIEnum()
    {
        StartCoroutine(GenericEffects.Shake(rTransform, shakeStrength, shakeTime));
        yield return new WaitForSeconds(shakeTime + 0.3f);
        LeanTween.moveY(gameObject, 30f, leaveTime).setEase(LeanTweenType.easeInCubic);
        StartCoroutine(outroCover.Cover(leaveTime / 2));
        yield return new WaitForSeconds(leaveTime);
        titleManager.LeaveScene();
    }
}
