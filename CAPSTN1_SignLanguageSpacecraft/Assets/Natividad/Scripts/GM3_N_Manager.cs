using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using TMPro;

using Random = UnityEngine.Random;

public class GM3_N_Manager : MonoBehaviour
{
    [Header("Files/ Prefabs")]
    public GameObject enemyPrefab;
    public TextAsset wordList;

    [Header("Components")]
    public GM3_N_Touch playerInput;
    public GM3_N_SignSpawner spawner;

    [Header("UI")]
    public TextMeshProUGUI scoreText;

    [Header("Misc Stuff idk i dont have time")]
    public float maxTimerValue;
    public float currentTimerValue;
    public int score = 0;

    [Header("Word Handlers")]
    public string currentWord;
    public List<string> availableWords;
    private string alphabet = "abcdefghijklmnopqrstuvwxyz";

    [Header("Enemy Handlers")]
    public int enemyIndex = 0;
    public int maxEnemyCount = 3;
    public List<GM3_N_Enemy> enemyList;
    public GM3_N_Enemy currentEnemy;
    public List<Transform> spawnPositions;

    private void OnEnable()
    {
        GM3_N_Touch.OnWordFormed += CompareWord;
    }

    private void OnDisable()
    {
        GM3_N_Touch.OnWordFormed -= CompareWord;
    }

    private void Awake()
    {
        ParseWordList();
        GetNewWord();
        NewWave();
    }

    private void Update()
    {
        currentTimerValue = Mathf.Clamp(currentTimerValue - Time.deltaTime, 0, Mathf.Infinity);
        if(currentEnemy != null) currentEnemy.SetTimerValue(currentTimerValue/maxTimerValue);
        if (currentTimerValue <= 0) { NewWave(); }
    }

    public void DestroyCurrentEnemy()
    {
        //GM3_N_Enemy toDestroy = currentEnemy;
        //Destroy(toDestroy.gameObject);
        currentEnemy.gameObject.SetActive(false);
    }

    public void NewWave()
    {
        enemyIndex = 0;
        SpawnNewEnemies(maxEnemyCount);
        TargetEnemy(enemyIndex);
        currentTimerValue = maxTimerValue;
    }

    public void DestroyEnemies()
    {
        if (enemyList.Count <= 0) return;
        for (int i = 0; i < enemyList.Count; i++)
            Destroy(enemyList[i].gameObject);
        enemyList.Clear();
    }

    public void SpawnNewEnemies(int spawnCount)
    {
        GM3_N_Enemy spawnedEnemy;
        DestroyEnemies();
        for(int i = 0; i < spawnCount; i++)
        {
            spawnedEnemy = Instantiate(enemyPrefab, spawnPositions[i].position, Quaternion.identity).GetComponent<GM3_N_Enemy>();
            enemyList.Add(spawnedEnemy);
        }
    }

    public void TargetEnemy(int index)
    {
        foreach (GM3_N_Enemy e in enemyList)
            e.SetTargeted(false);
        currentEnemy = enemyList[enemyIndex];
        currentEnemy.SetTargeted(true);
        currentEnemy.SetCurrentWord(currentWord);
    }

    public void CompareWord(string word)
    {
        if (currentWord != word) return;

        GetNewWord();
        DestroyCurrentEnemy();
        enemyIndex++;
        if (enemyIndex >= maxEnemyCount)
        {
            score++;
            scoreText.text = "Score: " + score;
            NewWave();
        }
        else
            TargetEnemy(enemyIndex);
    }

    void ParseWordList()
    {
        string wl = wordList.text;
        availableWords = new List<string>(Regex.Split(wl, "/"));
        availableWords.RemoveAt(availableWords.Count - 1);
    }

    void GetNewWord()
    {
        int index = Random.Range(0, availableWords.Count);
        
        currentWord = availableWords[index];
        spawner.SpawnSymbols(6, GenerateRandomString(6, currentWord));
    }

    List<Symbol> GenerateRandomString(int stringLength, string word)
    {
        string oldGen = word;
        int index;

        for (int i = 0; i < stringLength - word.Length; i++)
        {
            index = Random.Range(0, alphabet.Length);
            oldGen += alphabet[index].ToString();
        }

        Debug.Log(oldGen);
        return SymbolManager.instance.GetRandomSetFromString(oldGen);
    }
}
