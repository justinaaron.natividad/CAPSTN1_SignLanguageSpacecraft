using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NebulaTest : MonoBehaviour
{
    public Material mat;

    public Color correctColor;
    public Color neutralColor;
    public Color wrongColor;


    private void Start()
    {
        mat.SetColor("_Color", neutralColor);
        LeanTween.color(gameObject, correctColor, 2f);
    }

    private void Update()
    {
        
    }
}
