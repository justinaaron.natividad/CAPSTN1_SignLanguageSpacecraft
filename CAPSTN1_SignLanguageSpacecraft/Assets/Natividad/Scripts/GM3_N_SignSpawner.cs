using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM3_N_SignSpawner : MonoBehaviour
{
    [Header("Prefab")]
    public GameObject prefab; // Letter display prefab

    [Header("Parameters")]
    public int spawnNumber;   // Number of symbols spawned on the circle
    public float outerCircle; // Faded circle radius outer
    public float innerCircle; // Faded circle radius inner
    public float offset;      // Margin between two symbols

    [Header("Symbol List")]
    public List<GM3_N_SignDisplay> spawnedSymbols;

    private float maxSize;    // Max size calculated from the outer and inner margin + offset

    // Destroy all current symbols
    public void DestroySymbols()
    {
        foreach(Transform g in transform)
            Destroy(g.gameObject);
        spawnedSymbols.Clear();
    }

    // Reset highlight of all current symbols
    public void ResetSymbols()
    {
        foreach (GM3_N_SignDisplay g in spawnedSymbols)
            g.ResetDisplay();
    }

    // Spawn new set of symbols
    public void SpawnSymbols(int symbolNumber, List<Symbol> symbolList)
    {
        maxSize = outerCircle - innerCircle;
        spawnNumber = symbolNumber;

        GameObject spawned;
        Vector2 pos;
        DestroySymbols();

        // Get automatic spawn size
        float spawnRadius = innerCircle + maxSize / 2;
        float value = Mathf.Deg2Rad * (360 / symbolNumber);
        float spawnSize = Mathf.Clamp(Vector2.Distance(
            new Vector2(Mathf.Sin(0), Mathf.Cos(0)) * spawnRadius, 
            new Vector2(Mathf.Sin(value), Mathf.Cos(value)) * spawnRadius) - offset, 
            0, maxSize);

        // Spawn symbols
        for(int i = 0; i < symbolNumber; i++)
        {
            spawned = Instantiate(prefab, transform);
            pos.x = spawnRadius * Mathf.Sin(value * i);
            pos.y = spawnRadius * Mathf.Cos(value * i);
            spawned.GetComponent<GM3_N_SignDisplay>().Initialize(spawnSize, new Vector3(pos.x, pos.y, 0), symbolList[i]);
            spawnedSymbols.Add(spawned.GetComponent<GM3_N_SignDisplay>());
        }
    }
} 
