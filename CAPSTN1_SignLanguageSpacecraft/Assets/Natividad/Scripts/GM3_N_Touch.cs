using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GM3_N_Touch : MonoBehaviour
{
    public delegate void WordDelegate(string formedWord);
    public static WordDelegate OnWordFormed;

    public GM3_N_SignSpawner spawner;
    public GM3_N_SignDisplay selected;

    public TextMeshProUGUI testText;
    public string formedWord = "";
    public AudioClip selectSound;
    float pitch = 1;
    public float pitchIncrease;

    private Touch touch;

    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved)
                Drag();
            else if (touch.phase == TouchPhase.Ended)
                DragRelease();
        }
        testText.text = formedWord;
    }

    void Drag()
    {
        GM3_N_SignDisplay current;
        Vector3 dragPos = Camera.main.ScreenToWorldPoint(touch.position);
        if (selected != null && formedWord.Length < spawner.spawnNumber) selected.UpdateLine(dragPos);
        if (Physics2D.OverlapPoint(dragPos) == null) return;

        current = Physics2D.OverlapPoint(dragPos).gameObject.GetComponent<GM3_N_SignDisplay>();
        if (current != null && !current.highlighted)
        {
            if (selected != null) selected.UpdateLine(current.transform.position);
            selected = current;
            selected.StartCoroutine("Highlight");
            formedWord += selected.activeSymbol.ID().ToString();
            PlaySound();
        }
    }

    void DragRelease()
    {
        selected = null;
        pitch = 1;
        spawner.ResetSymbols();
        if(OnWordFormed != null && formedWord != "") OnWordFormed(formedWord);
        formedWord = "";
    }

    void PlaySound()
    {
        AudioManager.instance.ui.pitch = pitch;
        AudioManager.instance.ui.PlayOneShot(selectSound);
        pitch += pitchIncrease;
    }
}
