using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM4_N_Particle : MonoBehaviour
{
    [SerializeField] float particleSpeed;
    [SerializeField] float particleLifetime;
    [SerializeField] Color startColor;
    [SerializeField] Color endColor;

    void Start()
    {
        GetComponent<SpriteRenderer>().color = startColor;
        Destroy(gameObject, particleLifetime);
        LeanTween.moveY(gameObject, transform.position.y - (particleSpeed * particleLifetime), particleLifetime);
        LeanTween.color(gameObject, endColor, particleLifetime);
    }
}
