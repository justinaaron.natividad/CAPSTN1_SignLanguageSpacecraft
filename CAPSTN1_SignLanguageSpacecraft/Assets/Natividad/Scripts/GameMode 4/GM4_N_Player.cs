using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM4_N_Player : MonoBehaviour
{
    public delegate void GM4ExitDelegate(int starRating);
    public GM4ExitDelegate onExit;

    public HealthComponent health;
    [SerializeField] private GM4_N_ParticleSystem playerParticleSystem;

    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private int locationIndex = 1;
    [SerializeField] private List<Transform> lanePositions;
    [SerializeField] private Transform introPosition;
    [SerializeField] private bool isEntering = false;
    [SerializeField] private bool isInGame = false;
    [SerializeField] private float introLerpValue;
    [SerializeField] private float movementLerpValue;
    [SerializeField] private Vector3 currentPosition;
    [SerializeField] private float currentLerpValue;
    [SerializeField] private Image flash;

    [Header("Audio")]
    [SerializeField] private AudioClip explode;
    [SerializeField] private AudioClip warp;

    private void OnEnable()
    {
        GM4_N_Manager.OnIntroStart += OnIntroStart;
        GM4_N_Manager.OnMainGameStart += OnMainGameStart;
        GM4_N_Manager.OnOutroStart += OnOutroStart;
        GM4_N_Swipe.OnSwipe += ReadSwipe;
    }

    private void OnDisable()
    {
        GM4_N_Manager.OnIntroStart -= OnIntroStart;
        GM4_N_Manager.OnMainGameStart -= OnMainGameStart;
        GM4_N_Manager.OnOutroStart -= OnOutroStart;
        GM4_N_Swipe.OnSwipe -= ReadSwipe;
    }

    private void FixedUpdate()
    {
        if (!isEntering && !isInGame) return;
        transform.position = Vector2.Lerp(transform.position, currentPosition, currentLerpValue);
    }

    void ReadSwipe(int swipeValue)
    {
        if (!isInGame) return;
        locationIndex = Mathf.Clamp(locationIndex + swipeValue, 0, 2);
        currentPosition = lanePositions[locationIndex].position;
    }

    public void OnIntroStart()
    {
        isEntering = true;
        isInGame = false;
        playerParticleSystem.StartCoroutine("StartEmitting");
        currentPosition = introPosition.position;
        currentLerpValue = introLerpValue;
    }

    void OnMainGameStart()
    {
        isEntering = false;
        isInGame = true;
        playerParticleSystem.StopEmitting();
        currentPosition = lanePositions[1].position;
        currentLerpValue = movementLerpValue;
    }

    void OnOutroStart()
    {
        AudioManager.instance.environment.PlayOneShot(warp);
        StartCoroutine(OutroCoroutine());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Hit!");
        if (collision.gameObject.CompareTag("GM4_Asteroid"))
        {
            GenericEffects.Flash(flash, Color.red, 0.3f);
            AudioManager.instance.environment.PlayOneShot(explode);
            health.TakeDamage();
            StartCoroutine(Blink());
        }     
    }

    IEnumerator Blink()
    {
        for(int i = 0; i < 5; i++)
        {
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
            yield return new WaitForSeconds(0.1f);
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1);
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator OutroCoroutine()
    {
        isEntering = true;
        isInGame = false;
        playerParticleSystem.StartCoroutine("StartEmitting");
        currentPosition = lanePositions[1].position;
        currentLerpValue = introLerpValue;
        yield return new WaitForSeconds(0.5f);
        LeanTween.move(gameObject, transform.position + new Vector3(0, 20, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        isEntering = false;
        playerParticleSystem.StopEmitting();
        onExit(health.GetStarRating());
        
    }
}
