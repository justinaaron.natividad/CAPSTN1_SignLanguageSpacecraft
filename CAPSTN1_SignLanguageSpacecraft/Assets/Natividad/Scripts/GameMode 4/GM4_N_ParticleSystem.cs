using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM4_N_ParticleSystem : MonoBehaviour
{
    [SerializeField] GameObject particlePrefab;
    [SerializeField] bool isEmitting = false;
    [SerializeField] float emissionCooldown;

    public IEnumerator StartEmitting()
    {
        isEmitting = true;

        while (isEmitting)
        {
            Instantiate(particlePrefab, transform);
            yield return new WaitForSeconds(emissionCooldown);
        }
    }

    public void StopEmitting() { isEmitting = false; }
}
