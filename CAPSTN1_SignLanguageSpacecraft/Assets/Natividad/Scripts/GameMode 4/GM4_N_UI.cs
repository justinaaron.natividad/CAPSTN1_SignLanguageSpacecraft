using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GM4_N_UI : MonoBehaviour
{
    public delegate void GameStartDelegate();
    public static GameStartDelegate GameStart;

    [SerializeField] private List<GameObject> lifeUI;
    [SerializeField] private List<Image> symbolImages;
    [SerializeField] private GM4_N_Player player;
    [SerializeField] private Image cursor;
    [SerializeField] private Image timerBar;
    [SerializeField] private TextMeshProUGUI letterText;
    [SerializeField] private GameObject playerUI;
    [SerializeField] private GameObject fadePanel;

    [SerializeField] private GameObject symbolPrompt;
    [SerializeField] private GameObject swipeInstructions;
    [SerializeField] private bool isInGame;
    [SerializeField] private bool isFirstSwipe = true;
    [SerializeField] private float cursorHeight;

    private void OnEnable()
    {
        GM4_N_Manager.OnMainGameStart += OnMainGameStart;
        GM4_N_Swipe.OnSwipe += OnSwipe;
        player.health.OnHealthChanged += SetLife;
    }

    private void OnDisable()
    {
        GM4_N_Manager.OnMainGameStart -= OnMainGameStart;
        GM4_N_Swipe.OnSwipe -= OnSwipe;
        player.health.OnHealthChanged -= SetLife;
    }

    private void Update()
    {
        UpdateCursor();
    }

    void OnMainGameStart()
    {
        fadePanel.SetActive(true);
        playerUI.SetActive(true);
        swipeInstructions.SetActive(true);
        isInGame = true;
    }

    void OnSwipe(int value)
    {
        if (!isInGame || !isFirstSwipe) return;
        swipeInstructions.SetActive(false);
        GameStart();
        isFirstSwipe = false;
    }

    void UpdateCursor()
    {
        cursor.transform.position = new Vector3(player.transform.position.x, 0, 0);
        cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.localPosition.x, cursorHeight, 0);
    }

    public void ActivatePrompt(bool isActivated)
    {
        symbolPrompt.SetActive(isActivated);
    }

    public void UpdatePrompt(List<Symbol> symbols, int letterIndex)
    {
        for(int i = 0; i < symbolImages.Count; i++)
        {
            symbolImages[i].sprite = symbols[i].Image();
        }
        letterText.text = symbols[letterIndex].ID().ToString().ToUpper();
    }

    public void UpdateTimer(float time)
    {
        timerBar.fillAmount = time;
    }

    public void SetLife(int life)
    {
        for (int i = 0; i < lifeUI.Count; i++)
            lifeUI[i].SetActive(i <= life - 1);
    }
}
