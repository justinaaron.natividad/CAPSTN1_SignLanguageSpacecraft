using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM4_N_Manager : MonoBehaviour
{
    public delegate void IntroDelegate();
    public static IntroDelegate OnIntroStart;

    public delegate void MainGameDelegate();
    public static MainGameDelegate OnMainGameStart;

    public delegate void OutroDelegate();
    public static OutroDelegate OnOutroStart;

    [Header("Components")]
    [SerializeField] private GM4_N_Swipe swipe;
    [SerializeField] private GM4_N_Player player;
    [SerializeField] private GM4_N_UI ui;
    [SerializeField] private PreGameUI preGameUI;
    [SerializeField] private CompleteStar stars;
    [SerializeField] private List<GM4_N_Asteroid> asteroids;

    [Header("Parameters")]
    [SerializeField] private float introTime;
    [SerializeField] private float roundTime;
    [SerializeField] private float roundTimeScaling;
    [SerializeField] private float asteroidMoveTime;
    [SerializeField] private int maxRoundCount;
    [SerializeField] private int laneCount = 3;

    [Header("Audio")]
    [SerializeField] private AudioClip meteor;
    [SerializeField] private AudioClip bgm;
    [SerializeField] private OutroCover outroCover;

    private List<Symbol> symbols = new List<Symbol>();
    private int letterIndex;
    private int currentRoundCount = 0;
    private float currentRoundTime;

    [SerializeField] private DialogueTrigger dt;

    private void OnEnable()
    {
        PreGameUI.OnTutorialFinish += OnTutorialFinish;
        GM4_N_UI.GameStart += GameStart;
        player.health.OnDeath += OnLose;
        player.onExit += OnWin;
    }

    private void OnDisable()
    {
        PreGameUI.OnTutorialFinish -= OnTutorialFinish;
        GM4_N_UI.GameStart -= GameStart;
        player.health.OnDeath -= OnLose;
        player.onExit -= OnWin;
    }

    private void Start()
    {
        currentRoundTime = roundTime;
        AudioManager.instance.bg.clip = bgm;
        AudioManager.instance.bg.Play();
        dt.StartDisplay();
    }

    void GameStart()
    {
       GetSymbolSet();
    }

    private void Pause(bool isPaused)
    {
        swipe.SetEnabled(!isPaused);
        foreach (GM4_N_Asteroid a in asteroids)
            a.gameObject.SetActive(!isPaused);
    }

    private void OnLose()
    {
        Pause(true);
        preGameUI.SetPanel(Panels.Lose);
    }

    private void OnWin(int starRating)
    {
        Pause(true);
        stars.rating = starRating;
        preGameUI.SetPanel(Panels.Win);
        stars.StartCoroutine("StartAnimation");
    }

    public void OnTutorialFinish()
    {
        preGameUI.DisablePanels();
        StartCoroutine(IntroEnum());
    }

    IEnumerator IntroEnum()
    {
        OnIntroStart();
        yield return new WaitForSeconds(introTime);
        OnMainGameStart();
    }

    void GetSymbolSet()
    {
        symbols.Clear();
        symbols = SymbolManager.instance.GetRandomSet(laneCount);

        letterIndex = Random.Range(0, laneCount);
        ui.UpdatePrompt(symbols, letterIndex);
        ui.ActivatePrompt(true);

        StartCoroutine(StartRoundTimer());
    }

    IEnumerator StartRoundTimer()
    {
        float time = currentRoundTime;
        while (time > 0)
        {
            time -= Time.deltaTime;
            ui.UpdateTimer(time / currentRoundTime);
            yield return null;
        }
        ui.ActivatePrompt(false);
        StartCoroutine(ActivateAsteroids());
    }

    IEnumerator ActivateAsteroids()
    {
        AudioManager.instance.environment.PlayOneShot(meteor);
        currentRoundTime -= roundTimeScaling;
        for(int i = 0; i < asteroids.Count; i++)
        {
            if(i != letterIndex) asteroids[i].ActivateAsteroid(asteroidMoveTime);
        }
        yield return new WaitForSeconds(asteroidMoveTime);

        currentRoundCount++;
        if (player.health.GetHealthPercentage() > 0)
        {
            if (currentRoundCount >= maxRoundCount) { Pause(true); OnOutroStart(); }
            else GetSymbolSet();
        }
    }

    public void LoadScene(string gamemodeName)
    {
        StartCoroutine(LoadOutro(gamemodeName));
    }

    IEnumerator LoadOutro(string gamemodeName)
    {
        StartCoroutine(outroCover.Cover(0.5f));
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(gamemodeName);
    }

    public void OnComplete()
    {
        if (SaveManager.instance.saveState.lastCompleted < 3) SaveManager.instance.saveState.lastCompleted = 3;
        if (SaveManager.instance.saveState.L3_StarValue < player.health.GetStarRating()) SaveManager.instance.saveState.L3_StarValue = player.health.GetStarRating();
        SaveManager.instance.Save();
        LoadScene("MapScene");
    }
}
