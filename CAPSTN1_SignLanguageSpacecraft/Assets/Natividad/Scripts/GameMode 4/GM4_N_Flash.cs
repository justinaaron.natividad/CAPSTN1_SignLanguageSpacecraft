using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM4_N_Flash : MonoBehaviour
{
    [SerializeField] private Image flash;
    Color invisColor = new Color(0, 0, 0, 0);
    public void Flash(Color color, float flashTime)
    {
        flash.color = color;
        LeanTween.color(flash.rectTransform, invisColor, flashTime);
    }
}
