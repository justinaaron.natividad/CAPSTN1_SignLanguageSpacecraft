using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM4_N_ScrollingBackground : MonoBehaviour
{
    [SerializeField] private float scrollSpeed;
    [SerializeField] private float cutoffPoint;
    [SerializeField] private float yPos = 0;
    [SerializeField] private bool isScrolling;
    [SerializeField] private Color disabledColor;
    [SerializeField] private Color enabledColor;
    [SerializeField] private float colorChangeTime;
    [SerializeField] private List<Image> backgroundPieces;

    private void OnEnable()
    {
        GM4_N_Manager.OnIntroStart += OnIntroStart;
    }

    private void OnDisable()
    {
        GM4_N_Manager.OnIntroStart -= OnIntroStart;
    }

    private void Awake()
    {
        foreach(Image s in backgroundPieces)
        {
            s.color = disabledColor;
        }
    }

    void OnIntroStart()
    {
        ActivateBackground();
        StartCoroutine(StartScroll());
    }

    public void ActivateBackground()
    {
        foreach (Image s in backgroundPieces)
        {
            LeanTween.color(s.rectTransform, enabledColor, colorChangeTime);
        }
    }

    public IEnumerator StartScroll()
    {
        isScrolling = true;
        while (isScrolling)
        {
            yPos = (yPos + scrollSpeed * Time.deltaTime) % cutoffPoint;
            float y = yPos * -1;
            transform.localPosition = new Vector3(0, y, 0);
            yield return null;
        }
    }

    public void StopScroll()
    {
        isScrolling = false;
    }
}
