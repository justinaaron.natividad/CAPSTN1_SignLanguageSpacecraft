using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM4_N_Asteroid : MonoBehaviour
{
    [SerializeField] private GM4_N_ParticleSystem ps;
    [SerializeField] private Vector3 originalPosition;
    [SerializeField] private float moveDistance;

    private void Awake()
    {
        originalPosition = transform.position;
    }

    public void ActivateAsteroid(float moveTime)
    {
        Vector3 newPosition = originalPosition;
        newPosition.y = originalPosition.y - moveDistance;
        ps.StartCoroutine("StartEmitting");
        StartCoroutine(MoveAsteroid(newPosition, moveTime));
    }

    IEnumerator MoveAsteroid(Vector3 pos, float moveTime)
    {
        LeanTween.move(gameObject, pos, moveTime);
        yield return new WaitForSeconds(moveTime);
        ps.StopEmitting();
        transform.position = originalPosition;
    }
}
