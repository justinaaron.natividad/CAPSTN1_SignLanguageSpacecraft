using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM4_N_Swipe : MonoBehaviour
{
    public delegate void SwipeDelegate(int swipeValue);
    public static SwipeDelegate OnSwipe;

    [SerializeField] private float minimumSwipeMagnitude;
    [SerializeField] private float maximumTouchTime;

    private Touch touch;
    private float touchTime;
    private Vector2 startPos;
    private Vector2 endPos;
    private Vector2 swipeDirection;
    private bool isEnabled = true;

    // Update is called once per frame
    void Update()
    {
        if (!isEnabled) return;

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) DragStart();
            else if (touch.phase == TouchPhase.Ended) DragRelease();
            else Drag(); 
        }
    }

    void DragStart()
    {
        Vector2 dragPos = Camera.main.ScreenToWorldPoint(touch.position);
        startPos = dragPos;
    }

    void Drag()
    {
        Vector2 dragPos = Camera.main.ScreenToWorldPoint(touch.position);
        endPos = dragPos;
        touchTime += Time.deltaTime;
    }

    void DragRelease()
    {
        float temporaryTouchTime = touchTime;
        touchTime = 0;
        swipeDirection = endPos - startPos;

        if (swipeDirection.magnitude < minimumSwipeMagnitude || temporaryTouchTime > maximumTouchTime) { return; }
        if (swipeDirection.x > 0) OnSwipe(1);
        if (swipeDirection.x < 0) OnSwipe(-1);
    }

    public void SetEnabled(bool enabled) { isEnabled = enabled; }
}
