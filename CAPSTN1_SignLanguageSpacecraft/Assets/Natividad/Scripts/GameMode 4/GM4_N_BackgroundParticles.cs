using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM4_N_BackgroundParticles : MonoBehaviour
{
    [SerializeField] private List<ParticleSystem> particles;

    private void OnEnable()
    {
        GM4_N_Manager.OnIntroStart += OnIntroStart;
        //GM4_N_Manager.OnMainGameStart += OnMainGameStart;
    }

    private void OnDisable()
    {
        GM4_N_Manager.OnIntroStart -= OnIntroStart;
        //GM4_N_Manager.OnMainGameStart -= OnMainGameStart;
    }

    private void Awake()
    {
        foreach (ParticleSystem p in particles)
            p.Stop();
    }

    void OnIntroStart()
    {
        foreach (ParticleSystem p in particles)
            p.Play();
    }

    void OnMainGameStart()
    {
        // temp
    }
}
