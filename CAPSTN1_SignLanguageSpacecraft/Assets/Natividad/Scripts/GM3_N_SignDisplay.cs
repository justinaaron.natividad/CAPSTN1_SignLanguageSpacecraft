using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class GM3_N_SignDisplay : MonoBehaviour
{
    [Header("Symbol")]
    public Symbol activeSymbol;

    [Header("Components")]
    public CircleCollider2D col;
    public LineRenderer line;

    [Header("UI Components")]
    public Image highlight;
    public Image letterImage;

    [Header("Parameters")]
    public float popSize = 1.2f;
    public float popSpeed = 0.2f;
    public bool highlighted = false;

    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
        line = GetComponent<LineRenderer>();

        line.positionCount = 0;
        highlight.enabled = false;
        letterImage.enabled = false;
    }

    IEnumerator Highlight()
    {
        float curPop = popSize;
        highlighted = true;
        highlight.enabled = true;

        while(curPop > 1)
        {
            highlight.transform.localScale = Vector3.one * curPop;
            curPop -= popSpeed * Time.deltaTime;
            yield return null;
        }
        highlight.transform.localScale = Vector3.one;
    }

    public void Initialize(float size, Vector3 pos, Symbol s)
    {
        activeSymbol = s;
        GetComponent<RectTransform>().anchoredPosition = pos;

        highlight.GetComponent<RectTransform>().sizeDelta = new Vector2(size, size);
        letterImage.GetComponent<RectTransform>().sizeDelta = new Vector2(size, size);
        col.radius = (size / 2) * 0.8f;

        letterImage.sprite = activeSymbol.Image();

        highlight.enabled = false;
        letterImage.enabled = true;
    }

    public void UpdateLine(Vector3 linePos)
    {
        line.positionCount = 2;
        line.SetPosition(0, new Vector3(transform.position.x, transform.position.y, 0));
        line.SetPosition(1, new Vector3(linePos.x, linePos.y, 0));
    }

    public void ResetDisplay()
    {
        line.positionCount = 0;
        highlighted = false;
        highlight.enabled = false;
    }
}
