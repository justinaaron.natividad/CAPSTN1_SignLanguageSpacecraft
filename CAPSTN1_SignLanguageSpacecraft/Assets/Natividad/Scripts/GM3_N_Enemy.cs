using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GM3_N_Enemy : MonoBehaviour
{
    // Enemy parts
    public Image timerImage;
    public TextMeshProUGUI wordDisplay;

    public void SetTargeted(bool isTargeted)
    {
        wordDisplay.enabled = isTargeted;
        timerImage.enabled = isTargeted;
    }

    public void SetCurrentWord(string currentWord) 
    { 
        wordDisplay.text = currentWord;
    }

    public void SetTimerValue(float timerValue) 
    { 
        timerImage.fillAmount = timerValue;
    }
}
