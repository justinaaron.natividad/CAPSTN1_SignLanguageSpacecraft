using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LGM_Manager : MonoBehaviour
{
    [Header("Public Components")]
    public GM3_N_SignSpawner signSpawner;
    public LGM_EnemySpawner enemySpawner;
    public LGM_WordHelper wordHelper;
    public HealthComponent healthComponent;
    public LGM_Player player;

    [Header("UI Private Components")]
    [SerializeField] private DialogueTrigger dialogueTrigger;
    [SerializeField] private OutroCover outroCover;
    [SerializeField] private PreGameUI preGameUI;
    [SerializeField] private CompleteStar stars;
    [SerializeField] private Image flash;

    [Header("Score Parameters")]
    [SerializeField] private int levelThreshold;
    [SerializeField] private AudioClip damageSound;
    [SerializeField] private AudioClip bgm;
    private int score = 0;
    private int level = 0;

    private void OnEnable()
    {
        healthComponent.OnDeath += OnLose;
        LGM_Boss.OnBossDestroy += OnWin;
        PreGameUI.OnTutorialFinish += StartGame;
    }

    private void OnDisable()
    {
        healthComponent.OnDeath -= OnLose;
        LGM_Boss.OnBossDestroy -= OnWin;
        PreGameUI.OnTutorialFinish -= StartGame;
    }

    void Start()
    {
        AudioManager.instance.bg.clip = bgm;
        AudioManager.instance.bg.Play();
        dialogueTrigger.StartDisplay();
    }

    void StartGame()
    {
        preGameUI.DisablePanels();
        enemySpawner.GetNewWordList(level);
        enemySpawner.StartSpawning();
    }

    private void OnLose()
    {
        AudioManager.instance.ui.pitch = 1;
        preGameUI.SetPanel(Panels.Lose);
        enemySpawner.StopSpawning();
    }

    private void OnWin()
    {
        AudioManager.instance.ui.pitch = 1;
        enemySpawner.StopSpawning();
        stars.rating = healthComponent.GetStarRating();
        preGameUI.SetPanel(Panels.Win);
        stars.StartCoroutine("StartAnimation");
    }

    public void CheckEnemyDeath(bool destroyedByPlayer, LGM_Enemy enemy = null)
    {
        if (destroyedByPlayer)
        {
            score++;
            if (score % levelThreshold == 0)
            {
                level++;
                enemySpawner.GetNewWordList(level);
                if (level >= 3)
                {
                    enemySpawner.StopSpawning();
                    enemySpawner.SpawnBoss();
                }
            }
        }
        else TakeDamage();
    }

    public void TakeDamage()
    {
        healthComponent.TakeDamage();
        AudioManager.instance.environment.PlayOneShot(damageSound);
        GenericEffects.Flash(flash, Color.red, 0.3f);
        StartCoroutine(player.Blink(5));
    }

    public void LoadScene(string gamemodeName)
    {
        StartCoroutine(LoadOutro(gamemodeName));
    }

    IEnumerator LoadOutro(string gamemodeName)
    {
        StartCoroutine(outroCover.Cover(0.5f));
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(gamemodeName);
    }

    public void OnComplete()
    {
        if (SaveManager.instance.saveState.lastCompleted < 4) SaveManager.instance.saveState.lastCompleted = 4;
        if (SaveManager.instance.saveState.L4_StarValue < healthComponent.GetStarRating()) SaveManager.instance.saveState.L4_StarValue = healthComponent.GetStarRating();
        SaveManager.instance.Save();
        LoadScene("MapScene");
    }
}
