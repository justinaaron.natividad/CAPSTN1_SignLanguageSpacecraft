using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LGM_EnemySpawner : MonoBehaviour
{
    [SerializeField] private LGM_Manager manager;
    [SerializeField] private LGM_UI ui;
    [SerializeField] private LGM_Player player;

    [Header("Prefabs")]
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject bossPrefab;

    [Header("Required Objects")]
    [SerializeField] private Transform spawnHeight;
    [SerializeField] private Transform bossHeight;

    [Header("Spawn Parameters")]
    [SerializeField] private int verticalSections;
    [SerializeField] private float warpTime;
    [SerializeField] private float pauseTime;
    [SerializeField] private float bossPauseTime;
    [SerializeField] private List<float> spawnTimes;
    [SerializeField] private List<float> moveTimes;
    [SerializeField] private int level;
    [SerializeField] private bool isSpawning;

    private List<string> availableWords;
    private List<LGM_Enemy> enemies = new List<LGM_Enemy>();
    private Coroutine timerCoroutine;

    public void GetNewWordList(int level)
    {
        if (level > 3) return;
        this.level = level;
        availableWords = manager.wordHelper.ParseWordList(level);
    }

    public void StartSpawning()
    {
        if (timerCoroutine != null) StopCoroutine(timerCoroutine);
        SpawnEnemy();
    }

    public void StopSpawning()
    {
        if (timerCoroutine != null) StopCoroutine(timerCoroutine);
        foreach (LGM_Enemy e in enemies) { e.StopAllCoroutines();  e.DestroyEnemyRaw(); }
        enemies.Clear();
    }

    public IEnumerator SpawnCD()
    {
        yield return new WaitForSeconds(spawnTimes[level]);
        SpawnEnemy();
    }

    public void SpawnEnemy()
    {
        LGM_Enemy enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity).GetComponent<LGM_Enemy>();
        enemy.Initialize(manager.wordHelper.GetNewWord(availableWords), manager.player.transform, spawnHeight, player);
        enemy.OnEnemyDestroy += RetargetEnemy;
        enemy.OnEnemyDestroy += manager.CheckEnemyDeath;
        enemies.Add(enemy);
        RetargetEnemy(false);
        enemy.StartCoroutine(enemy.WarpIn(verticalSections, warpTime, pauseTime, moveTimes[level]));
        timerCoroutine = StartCoroutine(SpawnCD());
    }

    public void SpawnBoss()
    {
        LGM_Boss boss = Instantiate(bossPrefab, transform.position, Quaternion.identity).GetComponent<LGM_Boss>();
        boss.Initialize(manager.wordHelper.GetNewWordSet(3, availableWords), ui, manager, bossHeight, player);
        manager.player.focusedEnemy = boss.gameObject;
        boss.StartCoroutine(boss.WarpIn(warpTime, bossPauseTime));
    }

    public void RetargetEnemy(bool destroyedByPlayer, LGM_Enemy enemy = null)
    {
        if (enemy != null) enemies.Remove(enemy);
        if (!destroyedByPlayer && enemy != null) StopSpawning();
        if (enemies.Count > 0)
        {
            if (!enemies[0].isFocused)
            {
                enemies[0].SetIsFocused(true);
                manager.player.focusedEnemy = enemies[0].gameObject;
                manager.signSpawner.SpawnSymbols(6, manager.wordHelper.GenerateRandomString(6, enemies[0].enemyWord));
            }
        }
        else StartSpawning();
    }
}
