using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LGM_Boss : MonoBehaviour
{
    public delegate void BossDestroyDelegate();
    public static BossDestroyDelegate OnBossDestroy;
    [SerializeField] private LGM_Manager manager;

    [Header("Prefabs")]
    [SerializeField] private GameObject explosionPrefab;

    [Header("Components")]
    [SerializeField] private LGM_UI ui;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private GM1_N_ParticleSystem particle;
    [SerializeField] private TextMeshProUGUI wordText;
    [SerializeField] private Animator anim;
    [SerializeField] private LineRenderer laser;

    [Header("Parameters")]
    [SerializeField] private int lifePoints = 3;
    [SerializeField] private float fireTime;

    [Header("Materials")]
    [SerializeField] private Material flashMaterial;
    [SerializeField] private Material normalMaterial;
    [SerializeField] private Material outlineMaterial;
    [SerializeField] private AudioClip laserSound;
    [SerializeField] private AudioClip explosionSound;
    [SerializeField] private AudioClip hitSound;

    private Transform bossHeight;
    private bool isFiring = false;
    private float internalCooldown;
    private List<string> enemyWords;
    private LGM_Player player;

    private void OnEnable()
    {
        GM3_N_Touch.OnWordFormed += CompareWord;
    }

    private void OnDisable()
    {
        GM3_N_Touch.OnWordFormed -= CompareWord;
    }

    public void Initialize(List<string> words, LGM_UI ui, LGM_Manager manager, Transform bossHeight, LGM_Player player)
    {
        this.player = player;
        this.manager = manager;
        this.bossHeight = bossHeight;
        this.ui = ui;
        enemyWords = words;
        wordText.text = enemyWords[0];
        UpdateWord();
    }

    public void CompareWord(string word)
    {
        if (word == enemyWords[0])
        {
            player.StartCoroutine(player.Dodge());
            StartCoroutine(TakeDamage());
        }
        else
        {
            player.StartCoroutine(player.Laser());
        }
    }

    public void UpdateWord()
    {
        if (enemyWords.Count <= 0) return;
        wordText.text = enemyWords[0];
        manager.signSpawner.SpawnSymbols(6, manager.wordHelper.GenerateRandomString(6, enemyWords[0]));
    }

    public void DestroyBoss()
    {
        if (OnBossDestroy != null) OnBossDestroy();
        AudioManager.instance.environment.PlayOneShot(explosionSound);
        GM3_N_Touch.OnWordFormed -= CompareWord;
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject, 0.01f);
    }

    public IEnumerator WarpIn(float warpTime, float pauseTime)
    {
        Vector2 tpPos;
        StartCoroutine(particle.StartEmitting());
        ui.StartCoroutine(ui.StartWarning(warpTime + pauseTime, 3));
        sprite.material = flashMaterial;

        tpPos = new Vector2(0, bossHeight.position.y);
        LeanTween.move(gameObject, tpPos, warpTime).setEase(LeanTweenType.easeInOutQuint);
        yield return new WaitForSeconds(warpTime);
        anim.SetBool("isShooting", true);
        sprite.material = normalMaterial;
        yield return new WaitForSeconds(pauseTime);

        anim.SetBool("isShooting", false);
        particle.StopEmitting();
        StartCoroutine(StartFireCooldown());
    }

    public IEnumerator StartFireCooldown()
    {
        isFiring = true;
        internalCooldown = fireTime;
        while (isFiring)
        {
            if(internalCooldown > 0)
            {
                if (internalCooldown <= 1) SetIsShooting(true);
                else SetIsShooting(false);
                internalCooldown -= Time.deltaTime;
                yield return null;
            }
            else
            {
                Fire();
                internalCooldown = fireTime;
            }
        }
    }

    public void SetIsShooting(bool isShooting)
    {
        anim.SetBool("isShooting", isShooting);
        sprite.material = isShooting ? outlineMaterial : normalMaterial;
    }

    public void Fire()
    {
        manager.TakeDamage();
        SetIsShooting(false);
        StartCoroutine(ShootLaser(0.2f));
    }

    public IEnumerator TakeDamage()
    {
        isFiring = false;
        StartCoroutine(ShootLaser(0.2f));
        yield return new WaitForSeconds(0.2f);
        AudioManager.instance.environment.PlayOneShot(hitSound);
        enemyWords.RemoveAt(0);
        UpdateWord();

        lifePoints = (int)Mathf.Clamp(lifePoints - 1, 0, Mathf.Infinity);
        StartCoroutine(Flash(0.1f));
        if (lifePoints <= 0) { DestroyBoss();}
        
    }

    public IEnumerator Flash(float flashTime)
    {
        sprite.GetComponent<SpriteRenderer>().material = flashMaterial;
        yield return new WaitForSeconds(flashTime);
        sprite.GetComponent<SpriteRenderer>().material = normalMaterial;
        SetIsShooting(false);
        StartCoroutine(StartFireCooldown());
    }

    public IEnumerator ShootLaser(float time)
    {
        AudioManager.instance.environment.PlayOneShot(laserSound);
        laser.positionCount = 2;
        laser.SetPosition(0, transform.position);
        laser.SetPosition(1, transform.position - new Vector3(0, 20, 0));
        yield return new WaitForSeconds(time);
        laser.positionCount = 0;
    }
}
