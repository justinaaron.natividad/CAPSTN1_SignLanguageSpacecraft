using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security;
using TMPro;
using UnityEngine;

public class LGM_Enemy : MonoBehaviour
{
    public delegate void EnemyDestroyDelegate(bool destroyedByPlayer, LGM_Enemy self = null);
    public EnemyDestroyDelegate OnEnemyDestroy;

    [Header("Prefabs")]
    [SerializeField] private GameObject explosionPrefab;

    [Header("Components")]
    [SerializeField] private GameObject sprite;
    [SerializeField] private GM1_N_ParticleSystem particle;
    [SerializeField] private TextMeshProUGUI wordText;

    [Header("Colors")]
    [SerializeField] private Color unfocusedColor;
    [SerializeField] private Color focusedColor;
    [SerializeField] private AudioClip explosionSound;

    // Stats
    public bool isFocused;
    public string enemyWord;
    private Vector2 playerDirection;
    private Transform playerHeight;
    private Transform spawnHeight;
    private float xRange;
    private LGM_Player player;
    
    

    private void OnEnable()
    {
        if (isFocused) GM3_N_Touch.OnWordFormed = CompareWord;
    }

    private void OnDisable()
    {
        GM3_N_Touch.OnWordFormed -= CompareWord;
    }

    private void FixedUpdate()
    {
        playerDirection = transform.position - playerHeight.position;
        sprite.transform.up = playerDirection;
    }

    public void Initialize(string word, Transform playerHeight, Transform spawnHeight, LGM_Player player)
    {
        this.player = player;
        this.playerHeight = playerHeight;
        this.spawnHeight = spawnHeight;
        xRange = spawnHeight.position.x;
        enemyWord = word;
        wordText.text = word;
        SetIsFocused(false);
    }

    public void CompareWord(string word)
    {
        player.StartCoroutine(player.Laser());
        if (word == enemyWord)
        {
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            DestroyEnemy(true);
        }
    }

    public void SetIsFocused(bool isFocused)
    {
        this.isFocused = isFocused;
        wordText.gameObject.SetActive(isFocused);
        sprite.GetComponent<SpriteRenderer>().color = this.isFocused ? focusedColor : unfocusedColor;
        if(isFocused) GM3_N_Touch.OnWordFormed += CompareWord;
    }

    public void DestroyEnemy(bool destroyedByPlayer)
    {
        if (OnEnemyDestroy != null) OnEnemyDestroy(destroyedByPlayer, this);
        AudioManager.instance.environment.PlayOneShot(explosionSound);
        LeanTween.cancel(gameObject);
        GM3_N_Touch.OnWordFormed -= CompareWord;
        Destroy(gameObject, 0.01f);
    }

    public void DestroyEnemyRaw()
    {
        LeanTween.cancel(gameObject);
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        GM3_N_Touch.OnWordFormed -= CompareWord;
        Destroy(gameObject, 0.01f);
    }

    public IEnumerator WarpIn(int verticalSections, float warpTime, float pauseTime, float moveTime)
    {
        Vector2 spawnPos;
        StartCoroutine(particle.StartEmitting());

        spawnPos = new Vector2(Random.Range(-xRange, xRange), spawnHeight.position.y);
        LeanTween.move(gameObject, spawnPos, warpTime).setEase(LeanTweenType.easeInOutQuint);
        yield return new WaitForSeconds(warpTime + pauseTime);

        particle.StopEmitting();
        if(gameObject != null) StartCoroutine(MoveTowardsPlayer(verticalSections, moveTime));
    }

    public IEnumerator MoveTowardsPlayer(int verticalSections, float moveTime)
    {
        Vector3[] movePos = CreatePath(verticalSections);

        for(int i = 0; i < verticalSections - 1; i++)
        {
            LeanTween.move(gameObject, movePos[i], moveTime / verticalSections).setEase(LeanTweenType.easeInOutQuint);
            yield return new WaitForSeconds(moveTime / verticalSections);
        }
        LeanTween.move(gameObject, movePos[verticalSections - 1], moveTime / verticalSections).setEase(LeanTweenType.easeInOutBack);
    }

    public Vector3[] CreatePath(int verticalSections)
    {
        float moveY = spawnHeight.position.y - playerHeight.position.y;
        Vector3[] movePositions = new Vector3[verticalSections];

        for (int i = 0; i < verticalSections; i++)
        {
            movePositions[i] = new Vector3(Random.Range(-xRange, xRange), spawnHeight.position.y - (moveY / verticalSections) * i, 0f);
        }
        movePositions[verticalSections - 1] = playerHeight.position;

        return movePositions;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<LGM_Player>()) DestroyEnemy(false);
    }
}
