using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class LGM_WordHelper : MonoBehaviour
{
    [SerializeField] private TextAsset[] wordLists;
    private string alphabet = "abcdefghijklmnopqrstuvwxyz";

    public List<string> ParseWordList(int index)
    {
        string wl = wordLists[index].text;
        List<string> availableWords = new List<string>(Regex.Split(wl, "/"));
        availableWords.RemoveAt(availableWords.Count - 1);
        return availableWords;
    }

    public string GetNewWord(List<string> availableWords)
    {
        int index = Random.Range(0, availableWords.Count);
        string currentWord = availableWords[index];
        return currentWord;
    }

    public List<string> GetNewWordSet(int count, List<string> availableWords)
    {
        List<string> wordList = new List<string>(availableWords);
        List<string> returnList = new List<string>();

        for (int i = 0; i < count; i++)
        {
            int index = Random.Range(0, wordList.Count);
            returnList.Add(wordList[index]);
            wordList.Remove(wordList[index]);
        }

        return returnList;
    }

    public List<Symbol> GenerateRandomString(int stringLength, string word)
    {
        string oldGen = word;
        int index;

        for (int i = 0; i < stringLength - word.Length; i++)
        {
            index = Random.Range(0, alphabet.Length);
            oldGen += alphabet[index].ToString();
        }

        Debug.Log(oldGen);
        return SymbolManager.instance.GetRandomSetFromString(oldGen);
    }
}
