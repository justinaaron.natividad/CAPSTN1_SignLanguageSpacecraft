using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class LGM_UI : MonoBehaviour
{
    [SerializeField] private HealthComponent health;
    [SerializeField] private List<GameObject> lifeUI;

    [SerializeField] private GameObject warning;
    [SerializeField] private GameObject lifeBar;
    [SerializeField] private GameObject inputUI;

    private void OnEnable()
    {
        health.OnHealthChanged += SetLife;
    }

    private void OnDisable()
    {
        health.OnHealthChanged -= SetLife;
    }

    public void SetLife(int life)
    {
        if (life < 0) return;
        for (int i = 0; i < lifeUI.Count; i++)
            lifeUI[i].SetActive(i <= life - 1);
    }

    public IEnumerator StartWarning(float warningTime, int warningTimes)
    {
        lifeBar.SetActive(false);
        inputUI.SetActive(false);
        for (int i = 0; i < warningTimes; i++)
        {
            warning.SetActive(true);
            yield return new WaitForSeconds((warningTime/warningTimes)/2);
            warning.SetActive(false);
            yield return new WaitForSeconds((warningTime / warningTimes) / 2);
        }
        warning.SetActive(false);
        lifeBar.SetActive(true);
        inputUI.SetActive(true);
    }
}
