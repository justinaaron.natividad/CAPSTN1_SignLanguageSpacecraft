using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LGM_Explosion : MonoBehaviour
{
    [SerializeField] private float lifetime;
    private void Awake()
    {
        Destroy(gameObject, lifetime);
    }
}
