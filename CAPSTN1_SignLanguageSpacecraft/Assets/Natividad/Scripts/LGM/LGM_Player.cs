using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LGM_Player : MonoBehaviour
{
    public Transform playerAnchor;
    public GameObject sprite;
    public LineRenderer laser;
    public GameObject focusedEnemy;

    public float dodgeRangeX;
    public float dodgeClamp;
    public float dodgeRangeY;

    [SerializeField] private AudioClip laserSound;

    private float dodgeDirection;
    private Vector3 dodgePosition;
    private Vector3 playerDirection;
    private Vector3 playerPosition;
    private bool isDodging = false;

    /*private void OnEnable()
    {
        GM3_N_Touch.OnWordFormed += ShootLaser;
    }

    private void OnDisable()
    {
        GM3_N_Touch.OnWordFormed -= ShootLaser;
    }*/

    private void Awake()
    {
        laser.positionCount = 2;
    }

    private void FixedUpdate()
    {
        if (focusedEnemy != null)
        {
            playerDirection = focusedEnemy.transform.position - transform.position;
            transform.up = playerDirection;
        }
        else
        {
            transform.up = new Vector3(0, 1000, 0);
        }

        if (!isDodging)
        {
            playerPosition = playerAnchor.transform.position;
            playerPosition.z = 0;
            transform.position = playerPosition;
        }
    }

    public void ShootLaser()
    {
        if (focusedEnemy.GetComponent<LGM_Boss>()) StartCoroutine(Dodge());
        else StartCoroutine(Laser());
    }

    public IEnumerator Dodge()
    {
        isDodging = true;
        dodgeDirection = Random.Range(0, 1);
        dodgeDirection = dodgeDirection > 0.5f ? 1 : -1;
        dodgePosition = playerAnchor.position + new Vector3((dodgeClamp + Random.Range(0, dodgeRangeX)) * dodgeDirection, Random.Range(0, dodgeRangeY), 0);
        dodgePosition.z = 0;
        LeanTween.move(gameObject, dodgePosition, 0.2f).setEase(LeanTweenType.easeOutCubic);
        yield return new WaitForSeconds(0.2f);

        StartCoroutine(Laser());
        dodgePosition = playerAnchor.position;
        dodgePosition.z = 0;
        yield return new WaitForSeconds(0.2f);

        LeanTween.move(gameObject, dodgePosition, 0.2f).setEase(LeanTweenType.easeOutCubic);
        yield return new WaitForSeconds(0.2f);
        isDodging = false;
    }

    public IEnumerator Laser()
    {
        AudioManager.instance.environment.PlayOneShot(laserSound);
        if (focusedEnemy != null)
        {
            laser.positionCount = 2;
            laser.SetPosition(0, transform.position);
            laser.SetPosition(1, focusedEnemy.transform.position);
            yield return new WaitForSeconds(0.1f);
            laser.positionCount = 0;
        }
    }

    public IEnumerator Blink(int blinks)
    {
        for(int i = 0; i < blinks; i++)
        {
            sprite.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            yield return new WaitForSeconds(0.2f);
            sprite.GetComponent<SpriteRenderer>().color = Color.white;
            yield return new WaitForSeconds(0.2f);
        }
    }
}
