using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ResultsScreen : MonoBehaviour
{
    public GameObject starIcon;
    public GameObject starArea;
    public GameObject tryAgainText;
    //public SignScapesGameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {

        if (PlayerScript.currentHealthReference >= 0) DrawStars(PlayerScript.currentHealthReference);
        if (PlayerScript.currentHealthReference <= 0) tryAgainText.SetActive(true);
        //DrawStars(SignScapesGameManager.currentPlayerLives, SignScapesGameManager.maxPlayerLives);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Retry()
    {

        SceneManager.LoadScene("LeeGameMode");
    }

    public void DrawStars(int Stars)
    {

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i <= Stars - 1; i++)
        {
                GameObject resultingStars = Instantiate(starIcon, starArea.transform);

        }
    }
}
