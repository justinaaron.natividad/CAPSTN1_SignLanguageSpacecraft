using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignDisplay : MonoBehaviour
{
    public Symbol symbol;
    public SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    public void SetSymbol(Symbol s)
    {
        symbol = s;
        spriteRenderer.sprite = symbol.Image();
    }
}
