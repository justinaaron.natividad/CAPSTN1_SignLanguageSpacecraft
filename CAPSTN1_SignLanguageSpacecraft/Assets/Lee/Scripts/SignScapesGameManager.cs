using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;

public class SignScapesGameManager : MonoBehaviour
{
    [Header("Required classes")]
    [SerializeField] private GM3_N_Touch playerInput;
    [SerializeField] private GM3_N_SignSpawner spawner;
    [SerializeField] private TextAsset currentWordList;

    [Header("Game mode variables")]
    public int scoreCount;
    public int wordIndex;
    //public TextMeshProUGUI scoreText;

    //player loses lives after 3
    [Header("UI and Lists")]
    public List<TextAsset> wordsList;
    public TextMeshProUGUI scoreText;

    [Header("Word Handlers")]
    public string currentWord;
    public List<string> availableWords;
    private string alphabet = "abcdefghijklmnopqrstuvwxyz";

    [Header("Enemy variables")]
    public int enemyIndex = 0;
    public List<EnemySpaceShip> enemySpaceshipList;
    public EnemySpaceShip currentTarget;
    public EnemySpawner spawnerReference;
    public int maxEnemySpaceshipCount;
    public int remainingShips;

    [Header("Sound")]
    public AudioSource audioSource;
    public AudioClip explosionSound;

    private void OnEnable()
    {
        GM3_N_Touch.OnWordFormed += CompareWord;
    }

    private void OnDisable()
    {
        GM3_N_Touch.OnWordFormed -= CompareWord;
    }

    private void Awake()
    {
        ParseWordList(wordIndex);
        GetNewWord();
        
    }
    // Start is called before the first frame update
    void Start()
    {
        remainingShips = maxEnemySpaceshipCount;

        wordIndex = 0;
        //StartCoroutine(SetShipsActive());
        NewWaveTest();
    }

    // Update is called once per frame
    void Update()
    {

        NewWave();
    }

    #region Updated Methods
    void NewWaveTest()
    {

        StartCoroutine(spawnerReference.ConstantSpawning());
        enemyIndex = 0;
        TargetEnemy(enemyIndex);
    }

    void NewWave()
    {
        enemyIndex = 0;
        TargetEnemy(enemyIndex);
    }
    public void ResultsScreen ()
    {

        //Debug.Log("Dead");
        SceneManager.LoadScene("LeeResultScreen");
       
    }

   
    public void ResultsCheck()
    {
        
        if (remainingShips <= 0)
        {
            //foreach (EnemySpaceShip e in enemySpaceshipList) e.gameObject.SetActive(false);
            ResultsScreen();
            
        }
    }

    public void ClearShips()
    {

        foreach (EnemySpaceShip e in enemySpaceshipList) e.gameObject.SetActive(false);
    }
    #endregion

    #region Ripped off methods
    public void CompareWord(string word)
    {

        if (currentWord != word) return;

        GetNewWord();
        DestroyCurrentEnemy();
        scoreCount++;
        scoreText.text = scoreCount.ToString();
        NewWave();
        if(scoreCount % 4 == 0)
        {
            wordIndex++;
            ParseWordList(wordIndex);
        }
    }

    void ParseWordList(int index)
    {
        string wl = wordsList[index].text;
        currentWordList = wordsList[index];
        availableWords = new List<string>(Regex.Split(wl, "/"));
        availableWords.RemoveAt(availableWords.Count - 1);
    }

    public void GetNewWord()
    {
        int index = Random.Range(0, availableWords.Count);

        currentWord = availableWords[index];
        spawner.SpawnSymbols(6, GenerateRandomString(6, currentWord));
    }

    List<Symbol> GenerateRandomString(int stringLength, string word)
    {
        string oldGen = word;
        int index;

        for (int i = 0; i < stringLength - word.Length; i++)
        {
            index = Random.Range(0, alphabet.Length);
            oldGen += alphabet[index].ToString();
        }

        Debug.Log(oldGen);
        return SymbolManager.instance.GetRandomSetFromString(oldGen);
    }


    public void TargetEnemy(int index)
    {

        //Debug.Log("Targeted");
        foreach (EnemySpaceShip e in enemySpaceshipList)
            e.SetTargeted(false);
        currentTarget = enemySpaceshipList[enemyIndex];
        //currentTarget.gameObject.SetActive(true);
        currentTarget.SetTargeted(true);
        currentTarget.SetCurrentWord(currentWord);
    }

    public void DestroyCurrentEnemy()
    {

        audioSource.PlayOneShot(explosionSound);
        currentTarget.gameObject.SetActive(false);
        enemySpaceshipList.RemoveAt(enemyIndex);
        remainingShips--;
        ResultsCheck();
        Destroy(currentTarget.gameObject);

    }

    
    #endregion

}
