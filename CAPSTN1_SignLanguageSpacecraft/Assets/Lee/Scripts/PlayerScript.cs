using System.Collections;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    [SerializeField] private PlayerHealthUI healthUI;
    [SerializeField] private int currentHealth;
    [SerializeField] private SignScapesGameManager ssGM;
    [SerializeField] private SpriteRenderer sprite;

    public int maxHealth;
    public GameObject image;

    public static int maxHealthReference;
    public static int currentHealthReference;

    bool isDead;

    // Start is called before the first frame update
    void Start()
    {
        isDead = false;
        currentHealth = maxHealth;
        maxHealthReference = maxHealth;
        currentHealthReference = currentHealth;
        healthUI.DrawHealthIcons(currentHealth, maxHealth);
    }

    private void Update()
    {
        Vector3 direction = ssGM.currentTarget.transform.position - transform.position;
        //float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        //body.rotation = angle;
        image.transform.up = direction;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("EnemyShip"))
        {

            currentHealth--;
            currentHealthReference = currentHealth;
            Debug.Log(currentHealth);
            ssGM.GetNewWord();
            ssGM.DestroyCurrentEnemy();
            healthUI.DrawHealthIcons(currentHealth, maxHealth);

            if(currentHealth <= 0)
            {

                sprite.enabled = false;
                this.GetComponent<CircleCollider2D>().enabled = false;
                
                StartCoroutine(DestroyDelay());
            }
            
        }
    }

    IEnumerator DestroyDelay()
    {

        while (isDead == false)
        {
            yield return new WaitForSeconds(.5f);
            Debug.Log("Dead");
            ssGM.ResultsScreen();
            isDead = true;

        }
        
    }
}
