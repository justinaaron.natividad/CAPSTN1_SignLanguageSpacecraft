using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EnemySpaceShip : MonoBehaviour
{

    [Header("UI components")]
    public Image crosshairImage;
    public TextMeshProUGUI wordDisplay;

    [Header("Movement component")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private Rigidbody2D body;
    public Transform playerShipTransform;
    public GameObject image;
    private Vector2 movement;
    //public float visibilty;

 // Start is called before the first frame update
    void Start()
    {
        body = this.GetComponent<Rigidbody2D>();
    }

 // Update is called once per frame
    void Update()
    {
        Vector3 direction = playerShipTransform.position - transform.position;
        //float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        //body.rotation = angle;
        image.transform.up = -direction;
        direction.Normalize();
        movement = direction;
    }
    private void FixedUpdate()
    {
        CharacterMovement(movement);
    }
    #region Character Methods
    void CharacterMovement(Vector2 dir)
    {
        body.MovePosition((Vector2)transform.position + (dir * moveSpeed * Time.deltaTime));
    }

    public void SetTargeted(bool isTargeted)
    {
        wordDisplay.enabled = isTargeted;
        crosshairImage.enabled = isTargeted;
    }

    public void SetCurrentWord(string currentWord)
    {
        wordDisplay.text = currentWord;
    }

    #endregion
}
