using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField] private SignScapesGameManager gmReference;
    [SerializeField] private float spawnRate;
    public EnemySpaceShip enemySpaceship;
    public Transform spawnArea;
    //public List<GameObject> enemySpaceShips;
    public Transform playerTransform;
    EnemySpaceShip newShip;

    // Start is called before the first frame update
    void Start()
    {
        enemySpaceship.GetComponent<EnemySpaceShip>().playerShipTransform = playerTransform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnSpaceShipInBounds()
    {
        
        Bounds bounds = GetComponent<BoxCollider2D>().bounds;
        float xOffset = Random.Range(-bounds.extents.x, bounds.extents.x);
        float yOffset = Random.Range(-bounds.extents.y, bounds.extents.y);
        
        //gmReference.DestroyShips();
        newShip = GameObject.Instantiate(enemySpaceship);
        newShip.transform.position = new Vector2(bounds.center.x, bounds.center.y) + new Vector2(xOffset, yOffset);
        //newShip.gameObject.SetActive(false);
        gmReference.enemySpaceshipList.Add(newShip);
    }
    
    public IEnumerator ConstantSpawning()
    {

        for(int i = 0; i <= gmReference.maxEnemySpaceshipCount; i ++)
        {

            SpawnSpaceShipInBounds();

            yield return new WaitForSeconds(spawnRate);
        }
            
    }
}
