using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SignObjectScriptableObject", menuName = "SignedObject")]
public class SignObjects : ScriptableObject
{

    [SerializeField] private string objectToSign;
    [SerializeField] private Sprite objectImage;

    public string objectSign()
    {
        return objectToSign;
    }

    public Sprite image()
    {
        return objectImage;
    }
}
