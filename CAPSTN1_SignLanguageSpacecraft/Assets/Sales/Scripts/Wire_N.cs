using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


public class Wire_N : MonoBehaviour
{
    [Header("Wire Parts")]
    public BoxCollider2D boxCollider; // You had no reference for your collision detection
    public GameObject wireStart;      // wireStart must have a LineRenderer
    public GameObject wireEnd;        // new reference to wireEnd (You didn't need SpriteRenderer)

    public LineRenderer lr;

    public GameObject LightOn;

    private Vector3 draggingPos;
    private Touch touch;
    private Vector3 startPosition;
    private bool activated;           // this is to check if you pressed the wire or not
    public Material mat;
    private int currLines = 0;
    private bool isConnected = false;

    private void Awake()
    {
        lr = this.GetComponent<LineRenderer>();
    }

    void Start()
    {
        // dragStartPos removed because of new wireStart
        startPosition = wireEnd.transform.position; // save wire end's starting position
        
    }

    // Update is the same as previous script
    void Update()
    {
        // Touch code
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
                DragStart();
            else if (touch.phase == TouchPhase.Moved)
                Dragging();
            else if (touch.phase == TouchPhase.Ended)
                DragRelease();
        }
    }

    void DragStart()
    {

        /*if (lr == null)
        {
            CreateLine();
        }*/

        draggingPos = Camera.main.ScreenToWorldPoint(touch.position); // get position from touch to scene coordinate
        draggingPos.z = 0f;

        //lr.SetPosition(0, draggingPos);
        
        

        
        // Your script lacked a check to see which wire is being pulled
        // This makes it so that all wires are activated when you press anywhere

        // if statement to check if you pressed the wire or not
        if(boxCollider == Physics2D.OverlapPoint(draggingPos))
            activated = true;
        else
            activated = false;
    }

    void Dragging()
    {
        if (activated) // if wire is pressed during dragStart();
        {
            // Do drag code
            draggingPos = Camera.main.ScreenToWorldPoint(touch.position);
            draggingPos.z = 0f;

            if (lr == null)
            {
                CreateLine();
            }
            lr.SetPosition(0, startPosition);
            lr.SetPosition(1, draggingPos);

            CheckConnections();
            

            // I took out your old dragging code from here because it uses a foreach on Update()
            // and it might be the reason it's crashing your game

            UpdateWire(draggingPos);
        }
    }

    void DragRelease()
    {
        
        UpdateWire(startPosition);
   

        
            // add code here for connecting to socket
        lr.SetPosition(1, draggingPos);
        
        if (isConnected == true)
        {
            UpdateWire(this.transform.position);
        }
       
    }

    void CheckConnections()
    {
        //not working 100% need better way
        Collider2D[] colliders = Physics2D.OverlapCircleAll(draggingPos, .2f);
        foreach (Collider2D collider in colliders)
        {
            if (collider.gameObject != gameObject)
            {
                UpdateWire(collider.transform.position);

                //need to find a way to pull the symbols/equivalent letters into here
                if (transform.parent.name.Equals(collider.transform.parent.name))
                {
                    collider.GetComponent<Wire_N>()?.Done();
                    isConnected = true;
                    Done();
                }
                else
                {
                    isConnected = false;
                }
                return;
            }
        }
    }
    void UpdateWire(Vector3 draggingPos)
    {
        // Removed last two lines as theyre not needed anymore
        wireEnd.transform.position = draggingPos;

        // Update rotation of wireEnd
        Vector3 direction = wireEnd.transform.position - wireStart.transform.position;
        wireEnd.transform.right = direction;

        
    }

    void CreateLine()
    {
        lr = new GameObject("Line" + currLines).AddComponent<LineRenderer>();
        lr.material = mat;
        lr.positionCount = 2;
        lr.startWidth = 0.15f;
        lr.endWidth = 0.15f;
        //lr.useWorldSpace = false;
        lr.numCapVertices = 50;
    }

    void Done()
    {
        LightOn.SetActive(true);
        Destroy(this);
    }
}