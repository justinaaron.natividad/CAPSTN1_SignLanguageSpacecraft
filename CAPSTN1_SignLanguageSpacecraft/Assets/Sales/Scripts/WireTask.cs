using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireTask : MonoBehaviour
{
    //generation of random symbols here
    public List<Wire_N> leftWires = new List<Wire_N>();

    public List<Wire_N> rightWires = new List<Wire_N>();

    private List<int> availableLeftWireIndex;
    private List<int> availableRightWireIndex;
}
