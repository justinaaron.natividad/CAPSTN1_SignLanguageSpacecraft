using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wire : MonoBehaviour
{
    public SpriteRenderer wireEnd;
    public GameObject lightOn;
   
    Vector3 dragStartPos;
    Touch touch;
    Vector3 startPosition;
    
    // Start is called before the first frame update
    void Start()
    {
       dragStartPos = transform.parent.position;
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                DragStart();
            }

            if (touch.phase == TouchPhase.Moved)
            {
                Dragging();
            }

            if (touch.phase == TouchPhase.Ended)
            {
                DragRelease();
            }
        }
    }

    void DragStart()
    {
        dragStartPos = Camera.main.ScreenToWorldPoint(touch.position);
        dragStartPos.z = 0f;
       
    }
    void Dragging()
    {
        Vector3 draggingPos = Camera.main.ScreenToWorldPoint(touch.position);
        draggingPos.z = 0f;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(draggingPos, .2f);
        foreach(Collider2D collider in colliders)
        {
            if (collider.gameObject != gameObject)
            {
                UpdateWire(collider.transform.position);

                //need to find a way to pull the symbols/equivalent letters into here
                if (transform.parent.name.Equals(collider.transform.parent.name))
                {
                    collider.GetComponent<wire>()?.Done();
                    Done();
                }
                return;
            }
        }

        UpdateWire(draggingPos);
    }
    void DragRelease()
    {
        
        Vector3 dragReleasePos = Camera.main.ScreenToWorldPoint(touch.position);
        dragReleasePos.z = 0f;
        UpdateWire(startPosition);
    }

    void UpdateWire(Vector3 draggingPos)
    {
        transform.position = draggingPos;

        Vector3 direction = draggingPos - dragStartPos;
        transform.right = direction * transform.lossyScale.x;

        float dist = Vector2.Distance(dragStartPos, draggingPos);
        wireEnd.size = new Vector2(dist, wireEnd.size.y);
    }

    void Done()
    {
        lightOn.SetActive(true);

        Destroy(this);
    }
}
